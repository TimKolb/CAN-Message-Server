# Realtime CAN Monitor
Monitors the CAN Bus.

## Installation

### Prerequisites

* [homebrew](https://brew.sh/)
* [Node.js](https://nodejs.org/en/download/current/)
* [ansible](http://docs.ansible.com/ansible/intro_installation.html) 
* [fswatch](http://emcrisostomo.github.io/fswatch/) 
* [virtualbox](http://virtualbox.org)
* [vagrant](https://www.vagrantup.com)
 
```shell
brew install node ansible fswatch
brew cask install virtualbox vagrant
```

### Configure project

Clone the project, edit the config files and install and build the frontend application

```shell
git clone git@gitlab.com:TimKolb/CAN-Message-Server.git
cp client/src/config.js.dist client/src/config.js 
nano client/src/config.js # edit to your needs
nano server/config.js # edit to your needs
cd CAN-Message-Server/client
npm install
npm run build
cd ..
./deploy <your-raspberry-ssh-name>
```

### Setup Raspberry

Install and configure your raspberry automatically with ansible.  
Edit `/usr/local/etc/ansible/hosts` on your development machine.

```ini
[canmain]
ssh-host-name ansible_user=pi
```

and execute ansible playbook `raspberries.yml` in `.ansible`

```shell
cd .ansible
ansible-playbook raspberries.yml
```

On your raspberry or vagrant box: 

```shell
sudo -i
cd /src
npm install
```

### Start the application

On your raspberry or vagrant box:
```shell
npm start
```

## Develop

### Vagrant Box

To develop without an hardware raspberry you can run a VM.

```shell
brew cask install vagrant
vagrant plugin install vagrant-vbguest
 
vagrant up
vagrant ssh
cd /src
server/registerCAN.sh vcan0
npm start
```

To run frontend development server open another terminal on your machine and run

```shell
cd client
npm start
```

Go to [http://localhost:3000](http://localhost:3000) to view dev build.

Go to [http://localhost:8080](http://localhost:8080) to view prod build.

To stop the VM run `vagrant halt` from host system.

### Raspberry
To watch your file changes and deploy automatically run the following.

```shell
./watch your-raspberry-ssh-config-name
```

**OR**

To rsync your code to your raspberry run manually after every code change:

```shell
./deploy your-raspberry-ssh-config-name
```

**AND**

Connect to your raspberry and run

```shell
cd /src
npm start
```

Then open a `http://your-raspberrypi:8080` (Safari and Chrome tested).
Make sure that the raspberry and the client have a good wifi or ethernet connection.

## Contributors
* Kevin Kolberger
* Tim Kolberger

## License
© 2017