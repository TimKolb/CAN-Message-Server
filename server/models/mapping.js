const mongoose = require('mongoose');

module.exports = mongoose.Schema({
        cases: {type: Array},
        ecu: {type: Number, required: true},
        formula: {type: String},
        key: {type: String, required: true},
        stringType: {type: String},
        type: {type: String, required: true},
        unit: {type: String},
        note: {type: String},
        positions: {type: Object, required: true},
        addedManual: {type: Boolean, default: false}
    },
    {
        timestamps: true
    }
);