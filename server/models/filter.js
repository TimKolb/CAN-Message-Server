const mongoose = require('mongoose');

module.exports = mongoose.Schema({
        data: {type: Object, required: true},
        name: {type: String, required: true}
    },
    {
        timestamps: true
    }
);