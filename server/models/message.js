const mongoose = require('mongoose');

module.exports = mongoose.Schema({
        data: {type: Object, required: true},
        ecu: {type: Number, required: true},
        found: {type: Object},
        interval: {type: Number},
        sequencenr: {type: Number},
        timestamp: {type: Number, required: true},
        visible: {type: Boolean, default: true},
    },
    {
        timestamps: true
    }
);