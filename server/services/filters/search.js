let search = null;

module.exports = {
    name: 'SEARCH',
    validate(message) { // return false to filter message
        if (!search) {
            return true;
        }
        const array = [];
        let query = parseInt(search.data.query, search.data.searchType.base);
        while (query > 0) {
            array.unshift(query & 0xFF);
            query >>= 8;
        }
        message.found = [];

        let index = -1;
        do {
            index = message.data.indexOf(Buffer.from(array), ++index);
            if (index !== -1) {
                for (let i = index; i < index + array.length; i++) {
                    message.found.push(i);
                }
            }
        } while (index !== -1);
        return message.found.length !== 0;
    },
    /**
     *  item: {
     *      type: "SEARCH",
     *      data: {
     *          query: "23",
     *          searchType: {
     *              base: 16,
     *              key: "hex",
     *              label: "Hex"
     *          }
     *      }
     *  }
     */
    add(item) {
        search = item;
        console.log(`[SEARCH] ${JSON.stringify(search)}`);
        return search;
    },
    get() {
        return search ? [search] : [];
    },
    remove() {
        search = null;
    },
    clear() {
        search = null;
    }
};