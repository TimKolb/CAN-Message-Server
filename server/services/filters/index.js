const mongoose = require('mongoose');
const Filter = mongoose.model('filter', require('../../models/filter'));

const search = require('./search');
const ecu = require('./ecu');

const filters = [ecu, search];

function getActiveFilters() {
    let activeFilters = [];
    filters.forEach(filter => {
        activeFilters = activeFilters.concat(filter.get());
    });
    return activeFilters;
}

module.exports = {
    /**
     * Returns true if the value should be removed
     *
     * @param message
     * @returns {boolean}
     */
    filter(message) {
        return !filters.find(filter => !filter.validate(message));
    },

    /**
     * Adds a filter to given type
     *
     * @param name string filter type enum (SEARCH | HARDOVERLAY | SOFTOVERLAY ...)
     * @param data
     */
    add(name, data) {
        let filter = filters.find(filter => filter.name === name);

        if (!filter) {
            throw new Error(`No such filter: ${name}`);
        }

        let item = new Filter({name, data});
        filter.add(item);
        return item;
    },

    /**
     * Removes a filter from given type
     *
     * @param id from filter
     */
    remove(id) {
        let filter = filters.find(filter => filter._id === id);

        if (!filter) {
            throw new Error(`No such filter: ${type}`);
        }

        return filter.remove(id);
    },

    getFilters() {
        return getActiveFilters();
    },

    clear() {
      filters.forEach(filter => filter.clear());
    }
};