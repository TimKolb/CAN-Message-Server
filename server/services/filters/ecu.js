const filtername = 'ECU';
let filters = [];

/**
 *  e.g. filter {
 *      name: 'ECU'
 *      data: {
 *          ecu: Number
 *     }
 *  }
 */
module.exports = {
    name: filtername,
    validate(message) {
        // filter every message which ecu is in the filter array
        return !filters.find(filter => filter.data.ecu === message.ecu);
    },
    add(item) {
        filters.push(item);
        return item;
    },
    get() {
        return filters;
    },
    remove(id) {
        id = id._id || id;
        let index = filters.findIndex(filter => filter._id === id);
        if (index) {
            filters.splice(index, 1);
        }
    },
    clear() {
        filters = [];
    }
};