const EventEmitter = require('events');
const Promise = require('bluebird');
const mongoose = require('mongoose');
mongoose.Promise = Promise;
const can = require('socketcan');

const logger = require('../services/logger');
const config = require('../config');
const Message = mongoose.model('message', require('../models/message'));

const CHANNEL_STOPPED = 0;
const CHANNEL_STARTED = 1;
const CHANNEL_ERROR = -1;

/**
 *
 * @type {CanWorker}
 */
class CanWorker extends EventEmitter {
    constructor(canSocket) {
        super();
        this.canSocket = canSocket;
        this.channel = can.createRawChannel(canSocket, true);
        this.channel.addListener("onMessage", this.onMessage.bind(this));

        if (!this.channel) {
            this.channelStatus = CHANNEL_ERROR;
        } else {
            this.channelStatus = CHANNEL_STOPPED;
        }

        /** Stores the last Messages at matrix[ecu-id] */
        this.matrix = {};
    }

    /**
     * *Prepared* for multiple channels
     * @param name
     */
    getChannelStatus(name) {
        return this.channelStatus;
    }

    /**
     * Starts the CAN Bus
     *
     * @returns {CanWorker}
     */
    listen() {
        this.channel.start();
        this.channelStatus = CHANNEL_STARTED;
        logger.info(this.canSocket + ' channel started');

        return this;
    }

    /**
     * Stops the CAN Bus
     *
     * @returns {CanWorker}
     */
    stop() {
        this.channel.stop();
        this.channelStatus = CHANNEL_STOPPED;
        clearInterval(this.timer);
        logger.info(this.canSocket + ' channel stopped');
        return this;
    }

    /**
     * Callback function for new CAN Message
     *
     * @param canMsg
     */
    onMessage(canMsg) {
        /**
         * Message object looks like:
         *
         *  {
         *      ts_sec: 1472581582,
         *      ts_usec: 774458,
         *      id: 1091,
         *      data: <Buffer 00 13 00 74 79 00>
         *  }
         */

        let message = new Message({
            ecu: canMsg.id,
            visible: true,
            data: canMsg.data,
            timestamp: (canMsg.ts_sec * 1000) + (canMsg.ts_usec / 1000)
        });

        let messageHasChanged = false;

        let messageHistoryObject = this.matrix[message.ecu];
        if (!messageHistoryObject) {
            this.matrix[message.ecu] = {
                counter: 0
            };
        }
        let messageHistory = this.matrix[message.ecu].messages;
        if (!messageHistory) {
            this.matrix[message.ecu].messages = [];
            messageHasChanged = true;
        } else {
            let lastMessage = messageHistory[messageHistory.length - 1];
            message.set('interval', Math.ceil(message.timestamp - lastMessage.timestamp));
            if (Buffer.compare(message.data, lastMessage.data)) {
                messageHasChanged = true;
            }
        }
        message.sequencenr = ++this.matrix[message.ecu].counter;
        const messages = this.matrix[message.ecu].messages;
        messages.push(message);

        if (messages.length > config.historySize) {
            messages.splice(0, messages.length - config.historySize);
        }

        if (messageHasChanged) {
            this.emit('message', message);
        } else {
            this.emit('message.duplicate', message);
        }

    }
}

/**
 * Exports every time the same instance of CanWorker
 */
let canWorker = null;
module.exports = (socket) => {
    if (!canWorker) {
        if (!socket) {
            throw new Error('CanWorker is not initialized and no arguments were passed.')
        }
        canWorker = new CanWorker(socket);
    }

    return canWorker;
};
