const fs = require('fs');
const path = require('path');

const mongoose = require('mongoose');
const mkdirp = require('mkdirp');

const config = require('../config');
const logger = require('../services/logger');
const filters = require('../services/filters/index');

const definitionsPath = path.join(__dirname, '../definitions');
const cacheBasePath = path.join(__dirname, '../cache');

const Mapping = mongoose.model('mapping', require('../models/mapping'));

/**
 * Create the cache directory
 */
mkdirp.sync(cacheBasePath);

/**
 *
 * @type {Io}
 */
class Io {
    constructor(http) {
        this.latency = 0;
        this.counter = 0;
        this.io = require('socket.io')(http);
        this.matrix = {};
        this.streamStopped = false;
    }

    /**
     * Registers all socket io listeners
     */
    listen() {
        this.io.on('connection', this.onConnection.bind(this));
    }

    /**
     * Callback function for socket io connections
     *
     * @param socket
     */
    onConnection(socket) {
        logger.info('[IO] CANInterface connected', socket.id);

        const latencyStack = [];
        const latencyInterval = setInterval(() => {
            socket.emit('latency.request', Date.now(), (startTime) => {
                const latency = (Date.now() - startTime) / 2;
                const latHistory = 10;
                let latencyAverage = 0;

                latencyStack.push(latency);

                if (latencyStack.length > latHistory) {
                    latencyStack.splice(0, latencyStack.length - latHistory);
                }

                latencyStack.forEach((latency) => {
                    latencyAverage += latency;
                });

                latencyAverage = latencyAverage / latencyStack.length;

                socket.emit('latency.update', latencyAverage);
            })
        }, 1000);

        socket.on('stream.start', () => {
            logger.info('[IO] stream.start');
            this.streamStopped = false;
        });

        socket.on('stream.stop', () => {
            logger.info('[IO] stream.stop');
            this.streamStopped = true;
        });

        socket.on('filter.add', (filter, cb) => {
            logger.info('[IO] filter.add', JSON.stringify(filter));
            const item = filters.add(filter.type, filter.data);
            if (cb) {
                let activeFilters = filters.getFilters();
                logger.info('[IO] filter.add activeFilters:', JSON.stringify(activeFilters));
                cb(item, activeFilters);
            }
        });

        socket.on('filter.remove', (id, cb) => {
            logger.info('[IO] filter.remove');
            filters.remove(id);
            cb && cb(filters.getFilters());
        });

        socket.on('filter.clear', (cb) => {
            logger.info('[IO] filter.clear');
            filters.clear();
            cb && cb(filters.getFilters());
        });

        socket.on('filter.getAll', (cb) => {
            logger.info('[IO] filter.getAll');
            cb && cb(filters.getFilters());
        });

        socket.on('translations.get', (langKey, cb) => {
            logger.info('[IO] translations.get');
            const data = JSON.parse(fs.readFileSync(path.join(definitionsPath, `translations/${langKey}.json`), 'utf8'));
            cb && cb(data);
        });

        socket.on('meanings.getAll', (cb) => {
            const data = JSON.parse(fs.readFileSync(path.join(definitionsPath, `meanings.json`), 'utf8'));
            cb && cb(data);
        });

        socket.on('units.getAll', (cb) => {
            const data = JSON.parse(fs.readFileSync(path.join(definitionsPath, `conversionUnits.json`), 'utf8'));
            cb && cb(data);
        });

        socket.on('formulas.getAll', (cb) => {
            const data = JSON.parse(fs.readFileSync(path.join(definitionsPath, `formulas.json`), 'utf8'));
            cb && cb(data);
        });

        socket.on('canBusTypes.getAll', (cb) => {
            const data = JSON.parse(fs.readFileSync(path.join(definitionsPath, `canBusTypes.json`), 'utf8'));
            cb && cb(data);
        });

        socket.on('makes.getAll', (cb) => {
            const data = JSON.parse(fs.readFileSync(path.join(definitionsPath, `makes.json`), 'utf8'));
            cb && cb(data);
        });

        socket.on('mapping.getAll', (cb) => {
            logger.info('[mapping.getAll]', cb);
            Mapping.find({}, (err, allMappings) => !err && cb && cb(allMappings));
        });

        socket.on('mapping.get', (id, cb) => {
            logger.info('[mapping.get]');
            Mapping.findById(id, (err, mapping) => {
                if (err) {
                    return logger.error(err)
                }
                cb && cb(mapping)
            });
        });

        socket.on('mapping.save', (toUpdate, cb) => {
            logger.info('[mapping.save]', toUpdate);
            Mapping.findOneAndUpdate(
                {_id: toUpdate._id},
                toUpdate,
                {upsert: true, new: true},
                (err, updatedMapping) => {
                    if (err) {
                        return logger.error(err)
                    }
                    cb && cb(updatedMapping)
                }
            );
        });

        socket.on('mapping.delete', (toDelete, cb) => {
            logger.info('[mapping.delete]', toDelete);
            const {id} = toDelete;
            Mapping.findOneAndRemove(
                {id},
                err => {
                    if (err) {
                        return logger.error(err)
                    }
                    cb && cb()
                }
            );
        });

        socket.on('options.getAll', (cb) => {
            const cachePath = `${cacheBasePath}/options.json`;
            if (!fs.existsSync(cachePath)) {
                return cb && cb();
            }

            try {
                const file = fs.readFileSync(cachePath, 'utf8');
                const data = JSON.parse(file);
                cb && cb(data);
            } catch (e) {
                fs.unlink(cacheBasePath);
                cb && cb()
            }
        });

        socket.on('options.set', (options, cb) => {
            const cachePath = `${cacheBasePath}/options.json`;
            fs.writeFileSync(cachePath, JSON.stringify(options), 'utf8');
            cb && cb(options);
        });

        socket.on('disconnect', () => {
            logger.info('[IO] CANInterface disconnected', socket.id);
            clearInterval(latencyInterval);
        });

        socket.on('error', (err) => {
            logger.error(err);
        });
    };

    sendMessage(message) {
        if (this.streamStopped) {
            return null;
        }

        this.counter++;
        if (!this.matrix[message.ecu]) {
            this.matrix[message.ecu] = {
                messages: [message],
                interval: 0,
                sendCount: 0,
                skipCount: 0,
                count: 0,
                lastSent: 0
            };
        } else {
            this.matrix[message.ecu].messages.push(message);
            if (this.matrix[message.ecu].messages.length > config.historySize) {
                this.matrix[message.ecu].messages.splice(0, this.matrix[message.ecu].messages.length - config.historySize);
            }
        }

        this.matrix[message.ecu].count++;

        const lastMessages = this.matrix[message.ecu].messages;
        let lastMessage = null;
        if (lastMessages.length > 1) {
            lastMessage = lastMessages[lastMessages.length - 2];
        }

        const now = Date.now();
        const sendInterval = now - this.matrix[message.ecu].lastSent;
        if (sendInterval >= Math.max(this.latency, config.pufferTimePerECU) + (Object.keys(this.matrix).length / 2) &&
            ((lastMessage && lastMessage.visible && !message.visible) || message.visible)
        ) {
            this.matrix[message.ecu].lastSent = now;

            message.count = this.matrix[message.ecu].count;
            message.skipCount = this.matrix[message.ecu].skipCount;

            message.data = new Uint8Array(message.data);
            this.io.emit('message', message);
            this.matrix[message.ecu].sendCount++;
            this.matrix[message.ecu].skipCount = 0;
            this.matrix[message.ecu].messages = [];
        } else {
            this.matrix[message.ecu].skipCount++;
            //logger.info(message.ecu, this.matrix[message.ecu].interval, (this.matrix[message.ecu].skipCount / this.matrix[message.ecu].count) * 100 + '%');
        }
        return message;
    }
}

/**
 * Exports every time called the same instance of ioHandler
 */
let ioHandler = null;
module.exports = (http) => {
    if (!ioHandler) {
        if (!http) {
            throw new Error('IoHandler is not initialized and no arguments were passed.')
        }
        ioHandler = new Io(http);
    }
    return ioHandler;
};