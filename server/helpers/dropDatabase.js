const config = require('../config.js');
const mongoose = require('mongoose');
const Promise = require('bluebird');
const logger = require('../services/logger');

mongoose.Promise = Promise;

mongoose.connect(config.database, (err) => {
    if (err) {
        errorLog(err);
        return;
    }
    const Message = mongoose.model('message', require('./message'));
    const Snapshot = mongoose.model('snapshot', require('./snapshot'));

    Promise.all([
        Message.remove({}, errorLog),
        Snapshot.remove({}, errorLog)
    ]).then(() => {
        logger.warn('Database dropped!');
        process.exit();
    });
});

function errorLog(err) {
    if (err) {
        logger.error(err);
    }
}