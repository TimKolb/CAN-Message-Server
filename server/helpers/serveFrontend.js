const path = require('path');
const express = require('express');
const config = require('../config');
const logger = require('../services/logger');
const port = process.env.PORT || config.port;

module.exports = (http, app) => {
    app.use('/', express.static(path.join(__dirname, '/../../client/build')));

    app.get('/*', function (req, res) {
        res.sendFile(path.join(__dirname, '/../../client/build/index.html')); // load our public/index.html file
    });

    http.listen(port, () => {
            logger.info('Webserver listening on port ' + port);
        }
    );
};