const exec = require('child_process').exec;
const config = require('../config');
const logger = require('../services/logger');

module.exports = function registerCAN() {
// register vcan0 if not already done
    if (config.real) {
        exec('sudo ip a | grep ' + config.canSocket, (error, stdout, stderr) => {
            logger.info(`Register ${config.canSocket}`);
            if (error) {
                exec('sudo /sbin/ip link set ' + config.canSocket + " up type can bitrate " + config.baudrate, (error, stdout, stderr) => {
                    if (error) {
                        console.error(error);
                    }
                });
            }
        });
    }
    else {
        exec('sudo ip a | grep ' + config.vcanSocket, (error, stdout, stderr) => {
            if (error) {
                logger.info(`Register ${config.vcanSocket}`);
                exec('./server/registerCAN.sh ' + config.vcanSocket, (error, stdout, stderr) => {
                    if (error) {
                        console.error(error);
                    }
                });
            }
        });
    }
};