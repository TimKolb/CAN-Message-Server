const winston = require('winston');
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Rx = require('rx');

const app = express();
const http = require('http').Server(app);
const config = require('./config');
const logger = require('./services/logger');

const registerCan = require('./helpers/registerCAN');
const serveFrontend = require('./helpers/serveFrontend');

const CanWorker = require('./services/canWorker');
const filters = require('./services/filters/index');
const analytics = require('./services/analytics/index');
const Io = require('./services/io');

// Disable etag headers on responses
app.disable('etag');
app.disable('x-powered-by');

app.use(cors({credentials: true}));
registerCan();

// Connect to the database
mongoose.connect(config.database, (err) => {
    if (err) {
        logger.error(err);
        return;
    }
    logger.info('Database connection established');

    const canWorker = CanWorker(config.real ? config.canSocket : config.vcanSocket);
    const io = Io(http);

    canWorker.listen();
    io.listen();

    serveFrontend(http, app);

    const source = Rx.Observable.fromEvent(canWorker, 'message');
    source
        .filter(message => filters.filter(message))
        .subscribe(
            message => {
                const analysedMessage = analytics.analyse(message);
                if (config.saveAllMessages) {
                    analysedMessage.save();
                }
                io.sendMessage(analysedMessage.toObject());
            },
            err => logger.error(err)
        );

    /**
     * Process exit handling
     */
    process.stdin.resume(); // don't terminate program immediately
    function exitHandler(options, err) {
        logger.info('Prepare for exit ...');

        mongoose.disconnect();
        logger.info('Closed database connection');

        if (err) {
            logger.error(err.stack);
        }

        if (options.exit) {
            process.exit();
        }
    }

    //catches exit
    process.on('exit', exitHandler.bind(null, {}));
    //catches ctrl+c event
    process.on('SIGINT', exitHandler.bind(null, {exit: true}));
    //catches uncaught exceptions
    process.on('uncaughtException', exitHandler.bind(null, {exit: true}));

});