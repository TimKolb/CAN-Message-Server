module.exports = {
    database: 'mongodb://localhost/can-interface',
    real: false,                                    // true wenn can0
    baudrate: 500000,                             // baudrate wenn can0
    vcanSocket: 'vcan0',
    canSocket: 'can0',
    saveAllMessages: false,
    historySize: 10,
    analyseDepth: 10,
    pufferTimePerECU: 150, // 50 for production
    port: 8080
};