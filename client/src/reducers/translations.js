import {RECEIVE_TRANSLATIONS} from '../actions';

const initialState = {
    langKey: 'DE-de'
};

export function translations(state = initialState, action) {
    switch (action.type) {
        case RECEIVE_TRANSLATIONS:
            const {translations, langKey} = action;
            return {...state, [langKey]: translations};
        default:
            return state;
    }
}