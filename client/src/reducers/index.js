import {combineReducers} from 'redux';
import {stream} from './stream';
import {filter} from './filter';
import {meanings} from './meanings';
import {settings} from './settings';
import {translations} from './translations';
import {routerReducer} from 'react-router-redux';

export default combineReducers({
    stream,
    filter,
    meanings,
    settings,
    translations,
    routing: routerReducer
});
