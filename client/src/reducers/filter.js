import {
    ADD_FILTER,
    CLEAR,
    HARD_OVERLAY,
    HIDE_ECU,
    MARKER,
    RESTORE_FILTERS,
    SOFT_OVERLAY
} from '../actions/index';
const initialState = {
    all: [],
    marker: [],
    hideEcus: [],
    softOverlays: [],
    hardOverlays: []
};


export function filter(state = initialState, action) {
    switch (action.type) {
        case ADD_FILTER:
            return {
                ...state,
                all: action.filters
            };
        case SOFT_OVERLAY:
            const softOverlays = state.softOverlays.slice();
            softOverlays.push(action.softOverlay);
        return {
            ...state,
            softOverlays
        };
        case HARD_OVERLAY:
            const hardOverlays = state.hardOverlays.slice();
            hardOverlays.push(action.hardOverlay);
            return {
                ...state,
                hardOverlays
            };
        case HIDE_ECU:
            const hideEcus = state.hideEcus.slice();
            if (hideEcus.indexOf(action.ecu) < 0) {
                hideEcus.push(action.ecu);
            }
            return {
                ...state,
                hideEcus
            };
        case CLEAR:
            return {
                ...initialState
            };

        case RESTORE_FILTERS:
            return {
                ...state,
                all: action.filters,
            };
        case MARKER:
            const marker = state.marker.slice();
            marker.push(action.time);
            return {
                ...state,
                marker
            };
        default:
            return state;
    }
}