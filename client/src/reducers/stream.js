import {
    RECEIVE_MESSAGE,
    CLEAR_MESSAGES,
    STREAM_START,
    STREAM_STOP
} from '../actions';
import config from '../config';
import Message from '../models/Message';

const initialState = {
    started: true,
    messages: {}
};


export function stream(state = initialState, action) {
    switch (action.type) {
        case CLEAR_MESSAGES:
            return {
                ...state,
                messages: {}
            };
        case STREAM_START: {
            return !state.started ? {
                    ...state,
                    started: true
                } : state;
        }
        case STREAM_STOP: {
            return state.started ? {
                    ...state,
                    started: false
                } : state;
        }
        case RECEIVE_MESSAGE:
            const message = new Message(action.message);
            let lastMessages = state.messages[message.ecu];
            let lastMessage = null;

            if (Array.isArray(lastMessages)) {
                lastMessage = lastMessages[lastMessages.length - 1];
            }

            // transform message data
            message.data = Object.keys(message.data).map(index => {
                let value = message.data[index];
                if (typeof value === 'object') {
                    return value;
                }

                let bytePos = {
                    value,
                    marked: true
                };

                if (lastMessage) {
                    bytePos.marked = value !== lastMessage.data[index].value;
                }

                return bytePos;
            });

            let messages = [];
            // append message
            if (Array.isArray(state.messages[message.ecu])) {
                messages = state.messages[message.ecu].slice();
            }
            messages.push(message);

            // shorten message cache
            if (messages.length > config.cacheSize) {
                messages.splice(0, messages.length - config.cacheSize);
            }

            return {
                ...state,
                messages: {
                    ...state.messages,
                    [message.ecu]: messages
                }
            };
        default: {
            return state;
        }
    }
}