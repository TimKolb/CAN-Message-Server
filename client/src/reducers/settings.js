import {
    REQUEST_FORMULAS,
    REQUEST_MAKES,
    REQUEST_OPTIONS,
    REQUEST_UNITS,
    RECEIVE_UNITS,
    RECEIVE_FORMULAS,
    RECEIVE_MAKES,
    SET_OPTIONS,
    GET_OPTIONS
} from '../actions';

const initialState = {
    units: [],
    formulas: [],
    makes: [],
    options: {
        make: '',
        model: '',
        year: '0',
        vin: '',
        baudrate: '500000',
        busType: 'vcan0',
        extendedAddress: false,
        pin: 'high',
        busOrigin: 'powertrain'
    },
    isFetchingOptions: false,
    isFetchingUnits: false,
    isFetchingMakes: false,
    isFetchingFormulas: false
};

export function settings(state = initialState, action) {
    switch (action.type) {
        case REQUEST_OPTIONS:
            return {
                ...state,
                isFetchingOptions: true
            };
        case REQUEST_UNITS:
            return {
                ...state,
                isFetchingOptions: true
            };
        case REQUEST_MAKES:
            return {
                ...state,
                isFetchingMakes: true
            };
        case REQUEST_FORMULAS:
            return {
                ...state,
                isFetchingFormulas: true
            };
        case RECEIVE_UNITS:
            return {
                ...state,
                units: action.units,
                isFetchingUnits: false
            };
        case RECEIVE_FORMULAS:
            return {
                ...state,
                formulas: action.formulas,
                isFetchingFormulas: false
            };
        case RECEIVE_MAKES:
            return {
                ...state,
                makes: action.makes,
                isFetchingMakes: false
            };
        case SET_OPTIONS:
        case GET_OPTIONS:
            return {
                ...state,
                options: {
                    ...state.options,
                    ...action.options,
                },
                isFetchingOptions: false
            };
        default:
            return state;
    }
}