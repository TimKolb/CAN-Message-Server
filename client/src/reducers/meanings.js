import {RECEIVE_MEANINGS, RECEIVE_MAPPING} from '../actions';
import {FUNCTION, SWITCH} from '../models/Meaning';
import SwitchMeaning from '../models/SwitchMeaning';
import FunctionMeaning from '../models/FunctionMeaning';

const initialState = {
    all: [],
    mapped: []
};

export function meanings(state = initialState, action) {
    switch (action.type) {
        case RECEIVE_MEANINGS:
            const receivedMeanings = {};
            Object.keys(action.meanings).forEach(key => {
                const meaning = action.meanings[key];
                switch (meaning.type) {
                    case FUNCTION:
                        return receivedMeanings[key] = new FunctionMeaning(meaning);
                    case SWITCH:
                        return receivedMeanings[key] = new SwitchMeaning(meaning);
                    default:
                        return receivedMeanings[key] = meaning;
                }
            });

            return {
                ...state,
                all: receivedMeanings
            };
        case RECEIVE_MAPPING:
            const {mapping} = action;
            const mapped = [...state.mapped];
            const foundIndex = mapped.findIndex(item => item._id === mapping._id);

            if (foundIndex !== -1) {
                mapped[foundIndex] = mapped;
            } else {
                mapped.push(mapped);
            }

            return {
                ...state,
                mapped
            };
        default:
            return state;
    }
}