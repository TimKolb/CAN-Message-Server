import {socket} from './stream';

export const SEARCH = 'SEARCH';
export function search(data) {
    return function (dispatch) {
        dispatch(clearMessages());
        dispatch(addServerFilter({
            type: SEARCH,
            data: {
                query: data.query,
                searchType: data.searchType
            }
        }));
    }

}

export const HIDE_ECU = 'HIDE_ECU';
export function hideEcu(ecu) {
    return {
        type: HIDE_ECU,
        ecu
    }
}

export const SOFT_OVERLAY = 'SOFT_OVERLAY';
export function addSoftOverlay(softOverlay) {
    return {
        type: SOFT_OVERLAY,
        softOverlay
    }
}

export const HARD_OVERLAY = 'HARD_OVERLAY';
export function addHardOverlay(hardOverlay) {
    return {
        type: HARD_OVERLAY,
        hardOverlay
    }
}

export const CLEAR_MESSAGES = 'CLEAR_MESSAGES';
export function clearMessages() {
    return {
        type: CLEAR_MESSAGES
    }
}

export const ADD_FILTER = 'ADD_FILTER';
export function addServerFilter(data) {
    return function (dispatch) {
        socket.emit('filter.add', data, (filter, filters) => {
            dispatch({
                type: ADD_FILTER,
                filter,
                filters
            });
        });
    };
}

export const RESTORE_FILTERS = 'RESTORE_FILTERS';
export function restoreFilters() {
    return function (dispatch) {
        socket.emit('filter.getAll', filters => {
                dispatch({
                    type: RESTORE_FILTERS,
                    filters
                })
            }
        );
    };
}

export const CLEAR = 'CLEAR';
export function clearFilter() {
    return function (dispatch) {
        socket.emit('filter.clear', filters => {
                dispatch({
                    type: CLEAR,
                    filters
                })
            }
        );
    }
}

export const MARKER = 'MARKER';
export function markTime(time) {
    return {
        type: MARKER,
        time: time || Date.now()
    }
}

