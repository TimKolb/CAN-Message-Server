import {socket} from './stream';

export const REQUEST_FORMULAS = 'REQUEST_FORMULAS';
export const REQUEST_OPTIONS = 'REQUEST_OPTIONS';
export const REQUEST_MAKES = 'REQUEST_MAKES';
export const REQUEST_UNITS = 'REQUEST_UNITS';
function requestSettings(type) {
    return {
        type
    }
}

export const SET_HOLDTIME = 'SET_HOLDTIME';
export function setHoldTime(holdTime) {
    return {
        type: SET_HOLDTIME,
        holdTime
    }
}


export const RECEIVE_UNITS = 'RECEIVE_UNITS';
export function getUnits() {
    return function (dispatch) {
        dispatch(requestSettings(REQUEST_UNITS));
        socket.emit('units.getAll', ({units}) => {
            dispatch({
                type: RECEIVE_UNITS,
                units
            })
        })
    }
}

export const RECEIVE_FORMULAS = 'RECEIVE_FORMULAS';
export function getFormulas() {
    return function (dispatch) {
        dispatch(requestSettings(REQUEST_FORMULAS));
        socket.emit('formulas.getAll', ({formulas}) => {
            dispatch({
                type: RECEIVE_FORMULAS,
                formulas
            })
        })
    }
}

export const RECEIVE_MAKES = 'RECEIVE_MAKES';
export function getMakes() {
    return function (dispatch) {
        dispatch(requestSettings(REQUEST_MAKES));
        socket.emit('makes.getAll', ({makes}) => {
            dispatch({
                type: RECEIVE_MAKES,
                makes
            })
        })
    }
}

export const SET_OPTIONS = 'SET_OPTIONS';
export function setOptions(options) {
    return function (dispatch) {
        dispatch(requestSettings(REQUEST_OPTIONS));
        socket.emit('options.set', options, () => {
            dispatch({
                type: SET_OPTIONS,
                options
            });
        });
    }
}

export const GET_OPTIONS = 'GET_OPTIONS';
export function getOptions(options) {
    return function (dispatch) {
        dispatch(requestSettings(REQUEST_OPTIONS));
        socket.emit('options.getAll', options => {
            dispatch({
                type: GET_OPTIONS,
                options: options ? options : {}
            });
        });
    }
}