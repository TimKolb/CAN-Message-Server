import {socket} from './stream';

export const RECEIVE_TRANSLATIONS = 'RECEIVE_TRANSLATIONS';
export function getTranslations(langKey = 'DE-de') {
    return function (dispatch) {
        socket.emit('translations.get', langKey, translations => {
            dispatch({
                type: RECEIVE_TRANSLATIONS,
                langKey,
                translations
            })
        });
    }
}