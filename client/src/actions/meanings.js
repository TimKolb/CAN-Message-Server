import {socket} from './stream';

export const RECEIVE_MEANINGS = 'RECEIVE_MEANINGS';
export function getMeanings() {
    return function (dispatch) {
        socket.emit('meanings.getAll', ({meanings}) => {
            Object.keys(meanings).forEach(key => meanings[key].key = key);

            dispatch({
                type: RECEIVE_MEANINGS,
                meanings
            })
        })
    }
}

export const RECEIVE_MAPPING = 'RECEIVE_MAPPING';
export function saveMapping(mapping) {
    return function (dispatch) {
        socket.emit('mapping.save', mapping, (savedMapping) => {
            dispatch({
                type: RECEIVE_MAPPING,
                mapping: savedMapping
            });
        });
    }
}
