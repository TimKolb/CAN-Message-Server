import io from 'socket.io-client';
import config from '../config';
export const socket = io.connect(config.socketAddress);

export const RECEIVE_MESSAGE = 'RECEIVE_MESSAGE';
export function receiveMessage(message) {
    return {
        type: RECEIVE_MESSAGE,
        receivedAt: Date.now(),
        message
    };
}

export const OPEN_MODAL = 'OPEN_MODAL';
export function openModal(id) {
    return {
        type: OPEN_MODAL,
        id
    }
}

export const CLOSE_MODAL = 'CLOSE_MODAL';
export function closeModal(id) {
    return {
        type: CLOSE_MODAL,
        id
    }
}

export const STREAM_START = 'STREAM_START';
export function streamStart() {
    socket.emit('stream.start');
    return {
        type: STREAM_START
    }
}

export const STREAM_STOP = 'STREAM_STOP';
export function streamStop() {
    socket.emit('stream.stop');
    return {
        type: STREAM_STOP
    }
}
