import React from 'react';
import ValueDec from './ValueDec';

export default class Time extends React.Component {
    render() {

        return (
            <span className="time">
                <ValueDec className="time" byteObject={{value: this.props.value, marked: false}} bytes={2}/>
            </span>
        );
    }
}