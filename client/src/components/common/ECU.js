import React from 'react';

export default class ECU extends React.Component {
    static propTypes = {
        value: React.PropTypes.number.isRequired
    };

    render() {
        let valueString = this.props.value.toString(16);
        let format = (`00${valueString}`).substr(-3);
        return (
            <span className="ecuid">
                <code className="ecuid">{format}</code>
            </span>
        );
    }
}