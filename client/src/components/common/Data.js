import React from 'react';
import ValueHex from './ValueHex';
import ValueDec from './ValueDec';
import ValueASCII from './ValueASCII';
import ValueBin from './ValueBin';
import {blueA100} from 'material-ui/styles/colors';

export default class Data extends React.Component {

    static propTypes = {
        highlight: React.PropTypes.array,
        bitHighlight: React.PropTypes.object,
        highlightColor: React.PropTypes.string,
        type: React.PropTypes.oneOf(['hex', 'dec', 'decimal', 'bin', 'binary', 'ascii']),
        bytes: React.PropTypes.number,
        data: React.PropTypes.array.isRequired,
        byteStyle: React.PropTypes.object,
        onSelect: React.PropTypes.func
    };

    static defaultProps = {
        type: 'hex',
        bytes: 2,
        highlight: [],
        bitHighlight: {},
        highlightColor: blueA100
    };

    render() {
        const {highlight, highlightColor, bitHighlight, type, bytes, data, byteStyle, onSelect, selectedBytes, ...rest} = this.props;
        let children = data.map((item, key) => {
            let childStyle = {...byteStyle};
            if (highlight.indexOf(key) !== -1) {
                childStyle.color = highlightColor;
            }
            if (onSelect) {
                childStyle.cursor = 'pointer';
            }

            switch (type) {
                case 'hex':
                    return <ValueHex key={`hex-${key}`}
                                     byteObject={item}
                                     bytes={bytes}
                                     className={type}
                                     style={childStyle}
                                     selectedBytes={selectedBytes}
                                     position={key}
                                     onClick={onSelect ? () => onSelect(item, key) : null}/>;
                case 'decimal':
                case 'dec':
                    return <ValueDec key={`dec-${key}`}
                                     byteObject={item}
                                     bytes={bytes}
                                     className={type}
                                     style={childStyle}
                                     selectedBytes={selectedBytes}
                                     onClick={onSelect ? () => onSelect(item, key) : null}/>;
                case 'ascii':
                    return <ValueASCII key={`ascii-${key}`}
                                       byteObject={item}
                                       className={type}
                                       style={childStyle}
                                       selectedBytes={selectedBytes}
                                       onClick={onSelect ? () => onSelect(item, key) : null}/>;
                case 'binary':
                case 'bin':
                    return <ValueBin key={`bin-${key}`}
                                     byteObject={item}
                                     className={type}
                                     style={childStyle}
                                     bitHighlight={bitHighlight[key] || []}
                                     highlightColor={highlightColor}
                                     selectedBytes={selectedBytes}
                                     position={key}
                                     onClick={onSelect ? (bitPos) => onSelect(item, key, bitPos) : null}/>;
                default:
                    throw new Error('Unknown data type');
            }
        });

        return (
            <span {...rest}>
                {children}
            </span>
        );

    }
}

