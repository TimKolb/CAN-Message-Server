import React from 'react';

export default class DataBin extends React.Component {
    static defaultProps = {
        onClick: () => {
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            marked: !!this.props.byteObject.marked
        }
    }

    componentDidMount() {
        this.startTimer();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.byteObject.filtered) {
            this.setState({marked: false});
            this.stopTimer();
            return;
        }
        if (nextProps.byteObject.marked) {
            this.setState({marked: true});
            this.stopTimer();
            this.startTimer();
        }
    }

    componentWillUnmount() {
        this.stopTimer();
    }

    startTimer = () => {
        this.timer = setTimeout(() => {
            this.setState({marked: false});
        }, this.props.timeout || 2000);
    };

    stopTimer = () => {
        clearTimeout(this.timer);
    };

    render() {
        const {className, style, onClick, byteObject, bitHighlight, position} = this.props;
        const {marked} = this.state;
        const valueString = byteObject.value.toString(2);
        const paddedValueString = (`00000000${valueString}`).substr(valueString.length);

        let classNameMut = className ? className.toString() : '';
        classNameMut += marked ? ' highlighted' : '';

        return <span style={{flex: '0 0 auto', textAlign: 'center', padding: '20px 15px'}}>
            <div>{'ABCDEFGH'.split('')[position]}</div>
            {paddedValueString
                .split('')
                .map((char, index) => <code key={index}
                          className={bitHighlight && bitHighlight.indexOf(index) !== -1 ?
                              `${classNameMut} bit-highlighted` : classNameMut}
                          style={style}
                          onClick={() => onClick(index)}>
                        {char}
                    </code>)}
        </span>;
    }
}