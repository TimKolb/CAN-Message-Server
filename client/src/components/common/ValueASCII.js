import React from 'react';

export default class DataASCII extends React.Component {

    static defaultProps = {
        timeout: 2000
    };

    constructor(props) {
        super(props);
        this.state = {
            marked: !!this.props.byteObject.marked
        }
    }

    componentDidMount() {
        this.startTimer();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.byteObject.filtered) {
            this.setState({marked: false});
            this.stopTimer();
            return;
        }
        if (nextProps.byteObject.marked) {
            this.setState({marked: true});
            this.stopTimer();
            this.startTimer();
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.byteObject.value !== this.props.byteObject.value ||
            nextState.marked !== this.state.marked;
    }

    componentWillUnmount() {
        this.stopTimer();
    }

    startTimer = () => {
        this.timer = setTimeout(() => {
            this.setState({marked: false});
        }, this.props.timeout);
    };

    stopTimer = () => {
        clearTimeout(this.timer);
    };

    isValidASCII = (value) => (value < 127 && value > 32);

    render() {
        const {byteObject} = this.props;
        const {marked} = this.state;

        const value = (this.isValidASCII(byteObject.value)) ? byteObject.value : 0x5F;
        const ascii = String.fromCharCode(value);

        return (
            <code className={(marked) ? 'ascii highlighted' : 'ascii'}>
                {ascii}
            </code>
        );
    }
}