import React from 'react';

export default class DataDec extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            marked: !!this.props.byteObject.marked
        }
    }

    componentDidMount() {
        this.startTimer();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.byteObject.filtered) {
            this.setState({marked: false});
            this.stopTimer();
            return;
        }
        if (nextProps.byteObject.marked) {
            this.setState({marked: true});
            this.stopTimer();
            this.startTimer();
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.byteObject.value !== this.props.byteObject.value ||
            nextState.marked !== this.state.marked;
    }

    componentWillUnmount() {
        this.stopTimer();
    }

    startTimer = () => {
        // disabled for develop
        this.timer = setTimeout(() => {
            this.setState({marked: false});
        }, this.props.timeout || 2000);
    };

    stopTimer = () => {
        clearTimeout(this.timer);
    };

    render() {
        let {className, style, onClick} = this.props;
        const {marked} = this.state;

        className = className ? className.toString() : '';
        className += marked ? ' highlighted' : '';

        return (
            <code className={className} style={style} onClick={onClick}>
                {this.props.byteObject.value}
            </code>
        );
    }
}