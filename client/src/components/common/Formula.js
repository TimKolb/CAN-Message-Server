import React from 'react';
import {TextField} from 'material-ui';
import math from 'mathjs';

export default class Formula extends React.Component {
    static propTypes = {
        formula: React.PropTypes.string,
        onResult: React.PropTypes.func,
        onError: React.PropTypes.func,
        onFormulaChange: React.PropTypes.func,
    };

    static defaultProps = {
        onResult: () => {
        },
        onError: () => {
        },
        onFormulaChange: () => {
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            formula: props.formula || '',
            result: null,
            formulaError: null
        }
    }

    onFormulaChange = (formula) => {
        const {onFormulaChange} = this.props;
        onFormulaChange(formula);
        this.evalFormula(formula);
        this.setState({formula});
    };

    evalFormula = (formula) => {
        const {value} = this.props;
        const {onResult, onError} = this.props;

        try {
            const result = math.eval(formula, {x: value});
            onResult(result);
            this.setState({result, formulaError: ''});
        } catch (e) {
            onError(e);
            this.setState({formulaError: 'Formula is not valid', result: ''});
        }
    };

    render() {
        const {children} = this.props;
        const {formulaError, result, formula} = this.state;

        return <div>
            <TextField floatingLabelText="Formula"
                       value={formula}
                       errorText={formulaError}
                       onChange={(event, formula) => this.onFormulaChange(formula)}
                       multiLine
            />
            <div>
                {result !== undefined && <span>{result} {children}</span>}
            </div>
        </div>;
    }
}