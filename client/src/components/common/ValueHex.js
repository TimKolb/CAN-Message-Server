import React from 'react';

export default class DataHex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            marked: !!this.props.byteObject.marked
        }
    }

    componentDidMount() {
        this.startTimer();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.byteObject.filtered) {
            this.setState({marked: false});
            this.stopTimer();
            return;
        }
        if (nextProps.byteObject.marked) {
            this.setState({marked: true});
            this.stopTimer();
            this.startTimer();
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.byteObject.value !== this.props.byteObject.value ||
            nextState.marked !== this.state.marked ||
            nextProps.selectedBytes !== this.props.selectedBytes;
    }

    componentWillUnmount() {
        this.stopTimer();
    }

    startTimer = () => {
        this.timer = setTimeout(() => {
            this.setState({marked: false});
        }, this.props.timeout || 2000);
    };

    stopTimer = () => {
        clearTimeout(this.timer);
    };

    render() {
        let hexString = ('000000' + this.props.byteObject.value.toString(16)).substr(-this.props.bytes * 2);
        let {className, style, onClick, selectedBytes, position} = this.props;
        const {marked} = this.state;
        let classNameMut = className ? className.toString() : '';
        classNameMut += marked ? ' highlighted' : '';
        classNameMut += (Array.isArray(selectedBytes) && selectedBytes.indexOf(position) !== -1) ? ' byte-selected' : '';

        return (
            <code className={classNameMut}
                  style={style}
                  onClick={onClick}>
                {hexString}
            </code>
        );
    }
}