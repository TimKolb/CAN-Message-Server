import React from 'react';
import {FUNCTION, SWITCH} from '../../../models/Meaning';

import MapFunctionMeaning from './MapFunctionMeaning';
import MapSwitchMeaning from './MapSwitchMeaning';

export default function MeaningChooser(props) {
    const {meaning, ...restProps} = props;
    if (!meaning) {
        return null;
    }

    switch (meaning.type) {
        case FUNCTION:
            return <MapFunctionMeaning meaning={meaning} {...restProps} />;
        case SWITCH:
            return <MapSwitchMeaning meaning={meaning} {...restProps} />;
        default:
            return null;
    }
}