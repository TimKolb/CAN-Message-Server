import React from 'react';
import {connect} from 'react-redux';
import {TextField} from 'material-ui';

const styles = {
    container: {display: 'flex', justifyContent: 'space-around'},
    infoContainer: {display: 'flex', justifyContent: 'flex-start'},
    info: {marginRight: '15px'}
};

class MapSwitchMeaning extends React.Component {
    static propTypes = {
        meaning: React.PropTypes.object.isRequired,
        onChange: React.PropTypes.func
    };

    static defaultProps = {
        meaning: {},
        onChange: () => {
        }
    };

    onCaseChange = (caseItem, value) => {
        const {cases} = this.props.meaning;
        const caseIndex = cases.findIndex(item => item.state === caseItem.state);
        caseItem.value = value;
        if (cases[caseIndex]) {
            cases[caseIndex] = caseItem;
        }
        this.onMeaningChange({cases});
    };

    onMeaningChange = (delta) => {
        const {onChange, meaning} = this.props;
        const newMeaning = {...meaning, ...delta};
        onChange(newMeaning);
    };

    render() {
        const {meaning, translations} = this.props;
        if (!meaning || !translations) {
            return null;
        }

        const meaningTitle = translations.meanings[meaning.key].translation;
        const meaningDescription = translations.meanings[meaning.key].description;

        return <div style={styles.container}>
            <div>
                <div>{meaningTitle}</div>
                <div>{meaningDescription}</div>
            </div>
            <dl>
                <dt>Cases</dt>
                {meaning.cases.map((item, index) => <div key={index}>
                    <TextField
                        onChange={(event, value) => this.onCaseChange(item, value)}
                        floatingLabelText={`Value: ${translations.meanings[meaning.key].cases[item.state]}`}/>
                </div>)}
            </dl>
            <div style={styles.infoContainer}>
                <TextField
                    style={styles.info}
                    onChange={(event, note) => this.onMeaningChange({note})}
                    floatingLabelText="Note"
                    multiLine
                />
            </div>
        </div>
    };
}

const makeMapStateToProps = (initialState, initialProps) => {
    const {ecu} = initialProps;
    return state => ({
        ecu,
        message: state.stream.messages[ecu][0],
        translations: state.translations[state.translations.langKey]
    });
};

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    makeMapStateToProps,
    mapDispatchToProps
)(MapSwitchMeaning);