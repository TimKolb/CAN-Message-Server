import React from 'react';
import {connect} from 'react-redux';
import {TextField} from 'material-ui';
import Formula from '../../common/Formula';

const styles = {
    container: {display: 'flex', justifyContent: 'space-around'},
    infoContainer: {display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap'},
    info: {marginRight: '15px'}
};

class MapFunctionMeaning extends React.Component {
    static propTypes = {
        meaning: React.PropTypes.object,
        onChange: React.PropTypes.func
    };

    static defaultProps = {
        meaning: {},
        onChange: () => {
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            formula: '',
            result: '',
            formulaError: ''
        }
    }

    onMeaningChange = (delta) => {
        const {onChange, meaning} = this.props;
        const newMeaning = {...meaning, ...delta};
        onChange(newMeaning);
    };

    render() {
        const {meaning, translations} = this.props;
        const {result} = this.state;

        if (!meaning) {
            return null;
        }

        const meaningTitle = translations ? translations.meanings[meaning.key].translation : null;
        const meaningDescription = translations ? translations.meanings[meaning.key].description : null;

        return <div style={styles.container}>
            <div style={styles.infoContainer}>
                <div>{meaningTitle}</div>
                <div>{meaningDescription}</div>
                <TextField
                    style={styles.info}
                    value={meaning.unit}
                    onChange={(event, unit) => this.onMeaningChange({unit})}
                    floatingLabelText="Unit"
                />
                <TextField
                    style={styles.info}
                    value={meaning.stringType}
                    onChange={(event, stringType) => this.onMeaningChange({stringType})}
                    floatingLabelText="String Type"
                />
                <TextField
                    style={styles.info}
                    floatingLabelText="Note"
                    value={meaning.note}
                    onChange={(event, note) => this.onMeaningChange({note})}
                    multiLine
                />
            </div>
            <Formula formula={meaning.formula}
                     value={2}
                     onFormulaChange={(formula) => this.setState({formula})}
                     onResult={result => this.setState({result})}/>
            {result}
        </div>
    };
}

const makeMapStateToProps = (initialState, initialProps) => {
    const {ecu} = initialProps;
    return state => ({
        ecu,
        message: state.stream.messages[ecu][0],
        translations: state.translations[state.translations.langKey]
    });
};

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    makeMapStateToProps,
    mapDispatchToProps
)(MapFunctionMeaning);