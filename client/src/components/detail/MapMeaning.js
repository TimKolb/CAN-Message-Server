import React from 'react';
import {connect} from 'react-redux';
import {
    AutoComplete,
    Card,
    CardHeader,
    CardText,
    Paper,
    RaisedButton
} from 'material-ui';
import {FUNCTION, SWITCH} from '../../models/Meaning';
import MeaningChooser from './meanings/MeaningChooser';
import ECU from '../common/ECU';
import Data from '../common/Data';
import FunctionMeaning from '../../models/FunctionMeaning';
import {saveMapping} from '../../actions/meanings';

const styles = {
    container: {padding: '15px'},
    defs: {display: 'flex', justifyContent: 'space-between'},
    buttons: {marginLeft: 'auto'},
    save: {marginLeft: 'auto'},
    hexContainer: {
        display: 'flex',
        padding: '20px',
        justifyContent: 'space-around',
        flexDirection: 'column',
        whiteSpace: 'normal'
    },
    asciiContainer: {
        display: 'flex',
        justifyContent: 'space-around',
        padding: '20px',
    },
};

class MapMeaning extends React.Component {
    static propTypes = {
        ecu: React.PropTypes.number.isRequired,
        position: React.PropTypes.object.isRequired,
        onByteSelection: React.PropTypes.func
    };

    static defaultProps = {
        onByteSelection: () => {
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            type: SWITCH,
            meaning: '',
            expanded: true,
            selectedMeaning: new FunctionMeaning({addedManual: true}),
            selectedBitPositions: {}
        };
    }

    handleExpandChange = expanded => {
        this.setState({expanded: expanded});
    };

    onChangeMeaning = (selectedMeaning) => {
        this.setState({selectedMeaning});
    };

    onSelectType = type => {
        this.setState({type});
    };

    onMeaningSearch = searchText => {
        this.setState({searchText, selectedMeaning: new FunctionMeaning({addedManual: true})});
    };

    onSelectMeaning = (chosen, index) => {
        if (index < 0) {
            return this.setState({
                selectedMeaning: null
            });
        }
        const {meanings} = this.props;
        this.setState({
            selectedMeaning: meanings[chosen.value],
            type: meanings[chosen.value].type || this.state.type
        });
    };

    onSelectBitPos = (item, bytePos, bitPos) => {
        item = item || this.props.message.data[bytePos];
        const {selectedBitPositions} = this.state;
        let statePos = selectedBitPositions[bytePos];
        if (!Array.isArray(statePos)) {
            statePos = [];
        }

        const statePosIndex = statePos.indexOf(bitPos);
        if (bitPos === undefined) {

            // on Byte Select, toggle all 8 bit
            if (statePos.length > 0) {
                statePos = [];
            } else {
                statePos = [0, 1, 2, 3, 4, 5, 6, 7];
            }
        } else if (statePosIndex !== -1) {
            statePos.splice(statePosIndex, 1);
        } else {
            statePos.push(bitPos);
        }

        const update = {
            selectedBitPositions: {
                ...selectedBitPositions,
                [bytePos]: [...statePos],
            }
        };

        // remove empty arrays
        Object.keys(update.selectedBitPositions).forEach(bytePos => {
            if (update.selectedBitPositions[bytePos].length < 1) {
                delete update.selectedBitPositions[bytePos];
            }
        });
        this.setState(update);
    };

    getValueOfSelection = () => {
        //TODO: transform this.state.selectedBitPositions with this.props.message.data to its value
        return 42;
    };

    onSave = () => {
        const {ecu, dispatch} = this.props;
        const {type, selectedBitPositions, selectedMeaning} = this.state;
        const mapping = {
            ...selectedMeaning,
            ecu,
            type,
            positions: selectedBitPositions
        };
        console.log(mapping)
        dispatch(saveMapping(mapping));
    };

    render() {
        const {meanings, message, translations, ecu} = this.props;
        const {selectedMeaning, selectedBitPositions, expanded} = this.state;
        const meaningLabels = Object.keys(meanings).map(key => {
            if (translations && translations.meanings[key]) {
                return {
                    text: translations.meanings[key].translation,
                    value: key
                };
            } else {
                return '';
            }
        });

        return (
            <Card expanded={expanded}
                  onExpandChange={this.handleExpandChange}>
                <CardHeader showExpandableButton>
                    <div style={styles.hexContainer}>
                        <Data bytes={1}
                              data={message.data}
                              highlight={message.found}
                              selectedBytes={Object.keys(selectedBitPositions).map(i => parseInt(i, 10))}
                              type="hex"
                              style={{alignSelf: 'center'}}
                              byteStyle={{fontSize: '30px'}}
                              onSelect={this.onSelectBitPos}
                        />
                    </div>
                    <div style={styles.hexContainer}>
                        <Data data={message.data}
                              type={'bin'}
                              style={{
                                  alignSelf: 'center',
                                  display: 'flex',
                                  justifyContent: 'flex-start',
                                  flexWrap: 'wrap'
                              }}
                              bitHighlight={selectedBitPositions}
                              onSelect={this.onSelectBitPos}/>
                    </div>
                    <div style={styles.asciiContainer}>
                        <Data type="ascii" data={message.data} bytes={1}/>
                    </div>
                </CardHeader>
                <CardText expandable={true}>
                    <Paper style={styles.container}>
                        <div style={styles.defs}>
                            <dl style={{flexBasis: '100px'}}>
                                <dt>ECU</dt>
                                <dd><ECU value={ecu}/></dd>
                            </dl>
                            <dl>
                                <dt>Bit Positions</dt>
                                {Object.keys(selectedBitPositions)
                                    .map((key, index) => <dd key={index}>
                                        {key}: {selectedBitPositions[key].map((item, index) => item)}
                                    </dd>)}
                            </dl>
                            <div style={styles.buttons}>
                                <RaisedButton
                                    label={SWITCH}
                                    onClick={() => this.onSelectType(SWITCH)}
                                    disabled={this.state.type === SWITCH}
                                    buttonStyle={{borderTopRightRadius: '0', borderBottomRightRadius: '0'}}
                                    secondary/>
                                <RaisedButton
                                    label={FUNCTION}
                                    onClick={() => this.onSelectType(FUNCTION)}
                                    buttonStyle={{borderTopLeftRadius: '0', borderBottomLeftRadius: '0'}}
                                    disabled={this.state.type === FUNCTION}
                                    secondary/>
                            </div>
                        </div>
                        <AutoComplete
                            floatingLabelText="Meaning"
                            filter={AutoComplete.fuzzyFilter}
                            onUpdateInput={this.onMeaningSearch}
                            onNewRequest={this.onSelectMeaning}
                            dataSource={meaningLabels}
                            openOnFocus
                            menuStyle={{maxHeight: '300px'}}
                        />
                        <div>
                            <MeaningChooser
                                meaning={selectedMeaning}
                                ecu={ecu}
                                position={this.getValueOfSelection()}
                                message={message}
                                onChange={this.onChangeMeaning}/>
                        </div>
                        <div style={{display: 'flex'}}>
                            <RaisedButton
                                style={styles.save}
                                label="Save"
                                onClick={this.onSave}
                                primary/>
                        </div>
                    </Paper>
                </CardText>
            </Card>)
    }
}

const mapStateToProps = state => {
    return {
        meanings: state.meanings.all,
        translations: state.translations[state.translations.langKey]
    };
};

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(mapStateToProps, mapDispatchToProps)(MapMeaning);