import React from 'react';
import {Paper} from 'material-ui';
import {LineChart} from 'react-easy-chart';
import * as colors from 'material-ui/styles/colors';

export default (props) => {
    const {style, messages, windowWidth} = props;
    //value matrix
    const grid = messages.map(message => message.data.map(byte => byte.value));
    //rotate matrix to the left
    const lineData = grid.reduce((a, b, x) => b.map((value, index) => {
            let prev = a[index];
            if (!Array.isArray(prev)) {
                prev = [{y: prev, x}];
            }
            prev.push({y: value, x});
            return prev;
        }
    ));

    let lineCharts = lineData.map((dataset, index) => {
        return <Paper key={index} style={{margin: '10px 5px'}}>
            <div style={{
                fontSize: '14px',
                padding: '10px',
                textAlign: 'center',
                background: '#444',
                textTransform: 'uppercase'
            }}>
                Byte {index}
            </div>
            <LineChart
                key={index}
                yDomainRange={[0, 255]}
                data={[dataset]}
                width={windowWidth / (windowWidth < 600 ? 2 : 4)}
                lineColors={[colors.lightGreen500]}
                margin={{top: 2, right: 2, bottom: 2, left: 2}}
                interpolate={'cardinal'}/>
        </Paper>
    });

    return <div style={style}>
        {lineCharts}
    </div>
}