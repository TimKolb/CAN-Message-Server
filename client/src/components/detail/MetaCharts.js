import React from 'react';
import {
    Paper,
    CardText,
} from 'material-ui';
import {LineChart} from 'react-easy-chart';
import * as colors from 'material-ui/styles/colors';
import Time from '../common/Time';
const styles = {
    card: {
        display: 'flex',
        flexShrink: 0,
        padding: '10px'
    },
    cardTitle: {
        fontSize: '14px',
        padding: '10px 0',
        textAlign: 'center',
        background: '#444',
        textTransform: 'uppercase'
    },
    cardText: {
        padding: '0 16px 8px',
        fontSize: '35px'
    },
    stats: {
        flexBasis: '200px'
    }
};

export default (props) => {
    const {style, messages} = props;
    const message = messages[messages.length - 1];

    const sequencenrChartDatasets = messages.map((oldMessage, index) => ({y: oldMessage.sequencenr, x: index}));
    const sequencenrChart = <LineChart
        data={[sequencenrChartDatasets]}
        interpolate={'cardinal'}
        lineColors={[colors.lightBlue500]}
        axes
        yTicks={5}
        style={{'.tick text': {fill: '#FFF'}}}
        margin={{top: 10, right: 10, bottom: 10, left: 50}}/>;

    const intervalDatasets = messages.map((oldMessage, index) => ({y: oldMessage.interval, x: index}));
    const intervalChart = <LineChart
        data={[intervalDatasets]}
        interpolate={'cardinal'}
        lineColors={[colors.lightBlue500]}
        axes
        yTicks={5}
        style={{'.tick text': {fill: '#FFF'}}}
        margin={{top: 10, right: 10, bottom: 10, left: 50}}/>;

    return <div style={style}>
        <Paper>
            <div style={styles.cardTitle}>Interval</div>
            <div style={styles.card}>
                {intervalChart}
                <div>
                    <CardText style={styles.cardText}>
                        <Time value={message.interval}/>
                    </CardText>
                </div>
            </div>
        </Paper>
        <Paper>
            <div style={styles.cardTitle}>Counter</div>
            <div style={styles.card}>
                {sequencenrChart}
                <div>
                    <CardText style={styles.cardText}>
                        <code className="time">{message.sequencenr}</code>
                    </CardText>
                </div>
            </div>
        </Paper>
    </div>
}