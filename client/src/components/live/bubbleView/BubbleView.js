import React from 'react';
import BubbleGraph from './BubbleGraph';
import {Slider} from 'material-ui';

export default class BubbleView extends React.Component {
    constructor() {
        super();
        this.step = 25;
        this.state = {
            bubbleSize: 25
        }
    }

    handleBubbleSize = (event, value) => {
        this.setState({bubbleSize: value});
    };

    render() {
        let columnCount = this.state.bubbleSize / this.step + 1;

        return (
            <div style={{padding: '0 15px'}}>
                <Slider
                    min={0}
                    max={100}
                    step={this.step}
                    defaultValue={this.state.bubbleSize}
                    value={this.state.bubbleSize}
                    onChange={this.handleBubbleSize}
                />
                <BubbleGraph gridDistance={this.state.bubbleSize}
                             columnCount={columnCount}/>
            </div>
        );
    }
}