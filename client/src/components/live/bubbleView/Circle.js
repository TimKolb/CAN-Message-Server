import React from 'react';
import BubbleGraph from './BubbleGraph';

let style = {
    circle: {
        transition: 'r .1s cubic-bezier(0, 0, 0.2, 1)'
    }
};
export default class Circle extends React.Component {
    render() {
        const {bytePosition, byteObject} = this.props;

        // calculate the radius with the gridSize and maxValue of 2^(bits)
        const radius = Math.floor(Math.max(byteObject.value, 10) / 255 * 500);

        const color = BubbleGraph.valueToColor(byteObject.value);

        return (
            <g>
                <circle stroke={color}
                        fill={color}
                        cx={(bytePosition * 1000) + 500}
                        cy={500}
                        r={radius}
                        style={style.circle}
                />
            </g>
        )
    }
}