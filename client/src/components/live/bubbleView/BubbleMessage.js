import {connect} from 'react-redux';
import React from 'react';
import Circle from './Circle';
import ECU from '../../common/ECU';
import {push} from 'react-router-redux';

class BubbleMessage extends React.Component {
    toDetail = () => {
        const {dispatch, message} = this.props;
        dispatch(push(`ecu/${message.ecu}`));
    };

    render() {
        const {messages} = this.props;
        const message = messages[messages.length - 1];
        const children = message.data.map((item, key) => (
            <Circle key={key} bytePosition={key} byteObject={item}/>
        ));

        return (
            <figure onClick={this.toDetail} className="bubble-message-wrapper pointer">
                <div>
                    <ECU value={message.ecu}/>
                </div>
                <svg version="1.1"
                     className="bubble-message"
                     viewBox={'0 0 8000 1000'}>
                    {children}
                </svg>
            </figure>
        );
    }
}

const makeMapStateToProps = (initialState, initialProps) => {
    const {ecu} = initialProps;
    return state => ({
        messages: state.stream.messages[ecu]
    });
};

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    makeMapStateToProps,
    mapDispatchToProps
)(BubbleMessage);