import React from 'react';
import {connect} from 'react-redux';
import {
    Drawer,
    IconButton,
    Popover
} from 'material-ui';
import {push} from 'react-router-redux';
import ButtonClear from './action/ButtonClear';
import ButtonSearch from './action/ButtonSearch';
import Toggle from './action/Toggle';
import {markTime, addHardOverlay, addSoftOverlay, clearMessages} from '../../../actions';

import HardOverlay from 'material-ui/svg-icons/image/filter-none';
import SoftOverlay from 'material-ui/svg-icons/image/filter';
import Dynamic from 'material-ui/svg-icons/action/timeline';
import MatrixApply from 'material-ui/svg-icons/av/playlist-add-check';
import Send from 'material-ui/svg-icons/content/send';
import Settings from 'material-ui/svg-icons/action/settings';
import Marker from 'material-ui/svg-icons/device/location-searching';

export const appBarHeight = 48;
export const drawerWidth = 70;

export const styles = {
    icon: {
        width: drawerWidth
    },
    iconWrapper: {
        textAlign: 'center',
        borderBottom: '0px solid #AAA',
        paddingBottom: 15
    },
    drawer: {
        marginTop: appBarHeight,
        width: drawerWidth
    },
    iconText: {
        display: 'block',
        textTransform: 'uppercase',
        fontSize: '8px'
    },
    button: {
        margin: '0 auto',
        textAlign: 'center',
        width: 70,
        height: 'auto',
        padding: 0,
        border: 0
    },
    iconIcon: {
        width: 35,
        padding: '5px 0',
        height: 'auto',
    },
};

const buttons = [
    {
        icon: (<Marker style={styles.iconIcon}/>),
        text: 'Marker',
        action() {
            const {dispatch} = this.props;
            dispatch(markTime());
        }
    },
    {
        icon: <HardOverlay style={styles.iconIcon}/>,
        text: 'Hard Overlay',
        action() {
            const {dispatch, messages} = this.props;
            const hardOverlay = {};
            Object
                .keys(messages)
                .forEach(ecu => {
                    const latest = messages[ecu][messages[ecu].length - 1];
                    if (!Array.isArray(hardOverlay[ecu])) {
                        hardOverlay[ecu] = [];
                    }
                    latest.data.forEach((data, index) => data.marked ? hardOverlay[ecu].push(index) : null);
                });
            console.log('[ActionBar]', hardOverlay);
            dispatch(addHardOverlay(hardOverlay));
        }
    },
    {
        icon: <SoftOverlay style={styles.iconIcon}/>,
        text: 'Soft Overlay',
        action() {
            const {dispatch, messages} = this.props;
            const softOverlay = {};
            Object
                .keys(messages)
                .forEach(ecu => {
                    const latest = messages[ecu][messages[ecu].length - 1];
                    if (!Array.isArray(softOverlay[ecu])) {
                        softOverlay[ecu] = [];
                    }
                    latest.data.forEach((data, index) => data.marked ? softOverlay[ecu].push({
                            index,
                            value: data.value
                        }) : null);
                });
            console.log('[ActionBar]', softOverlay);
            dispatch(addSoftOverlay(softOverlay));
        }
    },
    {
        icon: (<Dynamic style={styles.iconIcon}/>),
        text: 'Dynamic',
        action() {
            const {dispatch} = this.props;
            dispatch(clearMessages());
        }
    },
    {
        icon: (<MatrixApply style={styles.iconIcon}/>),
        text: 'Matrix',
        action() {
            console.log('TODO Matrix');
        }
    },
    {
        icon: (<Send style={styles.iconIcon}/>),
        text: 'Keep alive',
        action() {
            console.log('TODO Keep alive');
        }
    },
    {
        icon: (<Settings style={styles.iconIcon}/>),
        text: 'Setup',
        action() {
            const {dispatch} = this.props;
            dispatch(push('/setup'));
        }
    }
];

class ActionBar extends React.Component {

    static propTypes = {
        messages: React.PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            anchorOrigin: {horizontal: "left", vertical: "center"},
            targetOrigin: {horizontal: "right", vertical: "center"}
        }
    }

    handleTouchTap = (event) => {
        // This prevents ghost click.
        event.preventDefault();
        this.setState({
            open: true,
            anchorEl: event.currentTarget,
        });
    };

    handleRequestClose = () => {
        this.setState({
            open: false
        });
    };

    render() {
        const mappedButtons = buttons.map((btn, index) =>
            <div key={`button-${index}`} style={styles.iconWrapper}>
                <IconButton
                    iconStyle={styles.icon}
                    style={styles.button}
                    onTouchTap={btn.action ? btn.action.bind(this) : () => {}}>
                    <div>
                        {btn.icon}
                        <span style={styles.iconText}>{btn.text}</span>
                    </div>
                </IconButton>
                {btn.popover ? <Popover
                        open={this.state.open}
                        anchorEl={this.state.anchorEl}
                        anchorOrigin={this.state.anchorOrigin}
                        targetOrigin={this.state.targetOrigin}
                        onRequestClose={this.handleRequestClose}>
                        {btn.popover}
                    </Popover> : null}
            </div>
        );

        return <Drawer
            open={this.props.active}
            openSecondary={true}
            width={styles.drawer.width}
            containerStyle={styles.drawer}
            {...this.props}>
            <div>
                <ButtonSearch />
                {mappedButtons}
                <Toggle />
                <ButtonClear />
            </div>
        </Drawer>
    }
}

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    mapDispatchToProps
)(ActionBar);
