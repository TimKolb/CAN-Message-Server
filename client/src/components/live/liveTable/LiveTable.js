import React from 'react';
import {connect} from 'react-redux';
import MessageLiveViewRow from './MessagesLiveViewRow';
import ActionBar, {drawerWidth} from './ActionBar';

const styles = {
    section: {
        marginRight: drawerWidth
    }
};

class LiveTable extends React.Component {

    static propTypes = {
        ecus: React.PropTypes.array,
        messages: React.PropTypes.object
    };

    render() {
        const {ecus, messages, active} = this.props;
        const rows = ecus.map(ecu => (<MessageLiveViewRow ecu={ecu} key={ecu}/>));

        return (<div>
            <section style={styles.section} className="live-view container-fluid">
                <ActionBar messages={messages} active={active}/>
                <div className="row header">
                    <span className="time" style={{textAlign: 'right'}}>Time <i>ms</i></span>
                    <span className="ecuid">CAN ID</span>
                    <span className="data">Data</span>
                    <span className="ascii" style={{textAlign: 'left'}}>ASCII</span>
                    <span className="counter" style={{textAlign: 'right'}}>Counter</span>
                </div>
                {rows}
            </section>
        </div>);
    }
}

const mapStateToProps = state => {
    const {stream, filter} = state;
    const ecus = Object
        .keys(stream.messages)
        .filter(ecu => filter.hideEcus.indexOf(parseInt(ecu, 10)) < 0);
    return {
        ecus,
        messages: stream.messages
    }
};

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LiveTable);