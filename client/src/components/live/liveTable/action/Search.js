import React from 'react';
import {
    Paper,
    RadioButtonGroup,
    RadioButton,
    RaisedButton,
    TextField
} from 'material-ui';
import {connect} from 'react-redux';
import {search} from '../../../../actions';

const HEX = {
    base: 16,
    key: 'hex',
    label: 'Hex'
};

const DEC = {
    base: 10,
    key: 'dec',
    label: 'Decimal'
};

const BIN = {
    base: 2,
    key: 'bin',
    label: 'Binary'
};

const ASCII = {
    base: 10,
    key: 'ascii',
    label: 'ASCII'
};

const FORMULA = {
    key: 'formula',
    label: 'Formula'
};

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            customFormula: '',
            formulaType: 'none',
            searchType: HEX,
            searchValue: '',
            searchValueError: '',
            customFormulaError: ''
        }
    }

    isValidValue(type, value) {
        //console.log(`[Search] Validate ${type}: ${value}`);
        switch (type.key) {
            case HEX.key:
                return /^[\da-f]+$/i.test(value) || value === '';
            case ASCII.key:
                return /^[\x20-\x7F]+$/.test(value) || value === '';
            case DEC.key:
                return /^[\d]+$/.test(value) || value === '';
            case BIN.key:
                return /^[01]+$/.test(value) || value === '';
            case FORMULA.key:
                return /^([-+/*]\d+(\.\d+)?)+$/.test(value) || value === '';
            default:
                throw new Error(`No such type: ${type}`);
        }
    }

    onSearchTypeChange = (event, value) => {
        //console.log(`[Search] Search type changed from ${this.state.searchType.label} to ${value.label}`);
        if (!this.isValidValue(this.state.searchType, this.state.searchValue)) {
            this.setState({
                searchType: value,
                searchValueError: `No valid ${this.state.searchType.label} input`
            });
            return;
        }

        let intermediateNumber = Number.parseInt(this.state.searchValue, this.state.searchType.base);
        let transformedValue = !Number.isNaN(intermediateNumber) ? intermediateNumber.toString(value.base) : '';

        this.setState({
            searchType: value,
            searchValueError: '',
            searchValue: transformedValue
        });
    };

    onSearchValueChange = (event, value) => {
        //console.log(`[Search] Search value changed from ${this.state.searchValue} to ${value}`);
        let searchValueError = '';
        if (!this.isValidValue(this.state.searchType, value)) {
            searchValueError = `No valid ${this.state.searchType.label} input`;
        }
        this.setState({searchValue: value, searchValueError});
    };

    onCustomFormulaChange = (event, value) => {
        //console.log(`[Search] Custom Formula changed from ${this.state.customFormula} to ${value}`);
        let customFormulaError = '';
        if (!this.isValidValue(FORMULA, value)) {
            customFormulaError = `No valid ${FORMULA} input`;
        }
        this.setState({customFormula: value, customFormulaError});
    };

    onFormulaTypeChange = (event, value) => {
        //console.log(`[Search] Formula type changed from ${this.state.formulaType} to ${value}`);
        let customFormulaError = '';
        if (!this.isValidValue(FORMULA, this.state.customFormula)) {
            customFormulaError = `No valid ${FORMULA} input`;
        }
        this.setState({formulaType: value, customFormulaError});
    };

    onSearchSubmit = () => {
        console.log(`[Search] Search submit`);
        const {dispatch} = this.props;
        dispatch(search({
            query: this.state.searchValue,
            searchType: this.state.searchType
        }));
    };

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            return this.onSearchSubmit();
        }
    };

    render() {
        return (
            <Paper>
                <div className="container-fluid" style={{paddingBottom: '10px'}}>
                    <div className="row">
                        <div className="col-xs-12">
                            <h3>Search</h3>
                        </div>
                        <div className="col-xs-8">
                            <TextField
                                style={{width: '100%'}}
                                floatingLabelText="Search for values"
                                value={this.state.searchValue}
                                onChange={this.onSearchValueChange}
                                errorText={this.state.searchValueError}
                                onKeyPress={this.handleKeyPress}
                                type={this.state.searchType === HEX ? 'text' : 'number'}/>
                        </div>
                        <div className="col-xs-4">
                            <RadioButtonGroup name="search-type"
                                              valueSelected={this.state.searchType}
                                              onChange={this.onSearchTypeChange}>
                                <RadioButton
                                    value={HEX}
                                    label={HEX.label}
                                />
                                <RadioButton
                                    value={DEC}
                                    label={DEC.label}
                                />
                                <RadioButton
                                    value={BIN}
                                    label={BIN.label}
                                />
                                <RadioButton
                                    value={ASCII}
                                    label={ASCII.label}
                                />
                            </RadioButtonGroup>
                        </div>
                    </div>
                    <div className="row" style={{padding: '20px 0'}}>
                        <div className="col-xs-8">
                            <TextField
                                disabled={this.state.formulaType !== 'custom'}
                                style={{width: '100%'}}
                                floatingLabelText="Custom formula"
                                value={this.state.customFormula}
                                onChange={this.onCustomFormulaChange}
                                errorText={this.state.customFormulaError}
                                onKeyPress={this.handleKeyPress}
                            />
                        </div>
                        <div className="col-xs-4">
                            <RadioButtonGroup name="use-formula"
                                              valueSelected={this.state.formulaType}
                                              onChange={this.onFormulaTypeChange}>
                                <RadioButton
                                    value="none"
                                    label="None"
                                />
                                <RadioButton
                                    value="all"
                                    label="All"
                                />
                                <RadioButton
                                    value="custom"
                                    label="Custom"
                                />
                            </RadioButtonGroup>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <RaisedButton
                                style={{float: 'right'}}
                                label="Search"
                                disabled={this.state.searchValueError !== '' || this.state.customFormulaError !== ''}
                                onTouchTap={this.onSearchSubmit}/>
                        </div>
                    </div>
                </div>
            </Paper>
        );
    }
}


const mapStateToProps = state => {
    return {}
};

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Search);