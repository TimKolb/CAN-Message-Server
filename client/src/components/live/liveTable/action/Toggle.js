import React from 'react';
import {connect} from 'react-redux';
import {
    IconButton,
    Popover
} from 'material-ui';
import Play from 'material-ui/svg-icons/av/play-circle-outline';
import Pause from 'material-ui/svg-icons/av/pause-circle-outline';
import {streamStart, streamStop} from '../../../../actions';
import {default as SearchPopup} from './Search';
import {styles} from '../ActionBar';

class Toggle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        };
    }
    handleTouchTap = (event) => {
        // This prevents ghost click.
        event.preventDefault();
        const {dispatch} = this.props;

        this.props.started ? dispatch(streamStop()) : dispatch(streamStart());
    };

    render() {
        const {started} = this.props;
        const {open} = this.state;

        let icon = started ?
            (<Pause style={styles.iconIcon}/>) :
            (<Play style={styles.iconIcon}/>);

        let text = started ?
            'Pause' :
            'Play';

        return (<div style={styles.iconWrapper}>
            <IconButton
                iconStyle={styles.icon}
                style={styles.button}
                onTouchTap={this.handleTouchTap}
            >
                <div>
                    {icon}
                    <span style={styles.iconText}>{text}</span>
                </div>
            </IconButton>
            <Popover
                open={open}
                anchorOrigin={{horizontal: "left", vertical: "top"}}
                targetOrigin={{horizontal: "right", vertical: "top"}}
                onRequestClose={this.handleRequestClose}
            >
                <SearchPopup onSearch={this.onSearch}/>
            </Popover>
        </div>);
    }
}

const mapStateToProps = state => ({
    started: state.stream.started
});

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Toggle);