import React from 'react';
import {IconButton, Badge} from 'material-ui';
import {connect} from 'react-redux';
import Clear from 'material-ui/svg-icons/maps/layers-clear';
import {clearFilter} from '../../../../actions';
import {styles} from '../ActionBar';

class ButtonClear extends React.Component {
    handleTouchTap = (event) => {
        // This prevents ghost click.
        event.preventDefault();
        const {dispatch} = this.props;
        dispatch(clearFilter());
    };

    render() {
        const {activeFiltersCount} = this.props;

        return (<div style={styles.iconWrapper}>
            <Badge
                badgeContent={activeFiltersCount}
                style={styles.button}
                badgeStyle={{color: '#FFF'}}
                primary={true}
            >
                <IconButton
                    iconStyle={styles.icon}
                    style={styles.button}
                    onTouchTap={this.handleTouchTap}
                >
                    <div>
                        <Clear style={styles.iconIcon}/>
                        <span style={styles.iconText}>Clear</span>
                    </div>
                </IconButton>
            </Badge>
        </div>);
    }
}

const mapStateToProps = state => {
    const {all, hideEcus, hardOverlays, softOverlays} = state.filter;
    return {
        activeFiltersCount: all.length + hardOverlays.length + softOverlays.length + hideEcus.length
    }
};

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ButtonClear);