import React from 'react';
import {
    IconButton,
    Popover
} from 'material-ui';
import Search from 'material-ui/svg-icons/action/search';
import {default as SearchPopup} from './Search';
import {styles} from '../ActionBar';


export default class ButtonSearch extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }

    handleTouchTap = (event) => {
        // This prevents ghost click.
        event.preventDefault();
        this.setState({
            open: true,
            anchorEl: event.currentTarget
        });
    };

    handleRequestClose = () => {
        this.setState({
            open: false
        });
    };

    render() {
        return (<div style={styles.iconWrapper}>
            <IconButton
                iconStyle={styles.icon}
                style={styles.button}
                onTouchTap={this.handleTouchTap}
            >
                <div>
                    <Search style={styles.iconIcon}/>
                    <span style={styles.iconText}>Search</span>
                </div>
            </IconButton>
            <Popover
                open={this.state.open}
                anchorOrigin={{horizontal: "left", vertical: "top"}}
                targetOrigin={{horizontal: "right", vertical: "top"}}
                onRequestClose={this.handleRequestClose}
            >
                <SearchPopup/>
            </Popover>
        </div>);
    }
}