import React from 'react';
import Data from '../../common/Data';
import ECU from '../../common/ECU';
import Time from '../../common/Time';
import {connect} from 'react-redux';
import Swipeout from 'rc-swipeout';
import {hideEcu} from '../../../actions';
import 'rc-swipeout/assets/index.css';
import {amberA700, redA700} from 'material-ui/styles/colors';
import {push} from 'react-router-redux';

class MessagesLiveViewRow extends React.Component {
    static defaultProps = {
        messages: []
    };

    render() {
        const {message} = this.props;
        return (
            <div className="message">
                <Swipeout
                    left={[
                        {
                            text: 'details',
                            onPress: () => {
                                const {dispatch} = this.props;
                                dispatch(push(`ecu/${message.ecu}`));
                            },
                            style: {backgroundColor: amberA700, color: 'white', margin: '-2px 0'}
                        }
                    ]}
                    right={[
                        {
                            text: 'hide',
                            onPress: () => {
                                const {dispatch} = this.props;
                                dispatch(hideEcu(message.ecu));
                            },
                            style: {backgroundColor: redA700, color: 'white'}
                        }
                    ]}
                    autoClose={true}>
                    <Time value={message.interval}/>
                    <ECU value={message.ecu}/>
                    <span className="data">
                        <Data type="hex" data={message.data} highlight={message.found} bytes={1}/>
                    </span>
                    <span className="ascii">
                        <Data type="ascii" data={message.data} bytes={1}/>
                    </span>
                    <span className="counter">
                        <code className="counter">{message.sequencenr}</code>
                    </span>
                </Swipeout>
            </div>
        );
    }
}

export const makeMapStateToProps = (initialState, initialProps) => {
    const {ecu} = initialProps;
    return state => {
        const {filter} = state;
        const messages = state.stream.messages[ecu];
        const message = messages[messages.length - 1];

        filter.softOverlays.forEach(softOverlay => {
            const softOverlayFilter = softOverlay[ecu];
            if (Array.isArray(softOverlayFilter)) {
                softOverlayFilter.forEach(
                    ({index, value}) => {
                        if (message.data[index].value === value) {
                            message.data[index].marked = false;
                            message.data[index].filtered = true;
                        }
                    });
            }
        });

        filter.hardOverlays.forEach(hardOverlays => {
            const hardOverlaysFilter = hardOverlays[ecu];
            if (Array.isArray(hardOverlaysFilter)) {
                hardOverlaysFilter.forEach((index) => {
                    message.data[index].marked = false;
                    message.data[index].filtered = true;
                });
            }
        });

        return {
            message,
            filter
        }
    };
};

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    makeMapStateToProps,
    mapDispatchToProps
)(MessagesLiveViewRow);