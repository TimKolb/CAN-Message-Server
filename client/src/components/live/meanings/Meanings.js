import React from 'react';
import {connect} from 'react-redux';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn
} from 'material-ui';

class Meanings extends React.Component {
    static defaultProps = {
        meanings: []
    };

    render() {
        const {translations, meanings} = this.props;
        if (!translations || !meanings) {
            return <div></div>;
        }
        let listItems = Object.keys(meanings)
                .map((key, listItemIndex) => {
                    const meaning = meanings[key];
                    switch (meaning.type) {
                        case 'FUNCTION':
                            return <TableRow
                                key={listItemIndex}>
                                <TableRowColumn>{meaning.key}</TableRowColumn>
                                <TableRowColumn>
                                    {translations.meanings[key].description}
                                </TableRowColumn>
                                <TableRowColumn>{meaning.type}</TableRowColumn>
                                <TableRowColumn>{meaning.stringType} | {meaning.unit}</TableRowColumn>
                                <TableRowColumn>VALUE</TableRowColumn>
                            </TableRow>;
                        case 'SWITCH':
                            let cases = meaning.cases.map((item, caseIndex) => {
                                return <span
                                    className="case-wrapper"
                                    key={`${listItemIndex}-${caseIndex}`}>
                                <div
                                    className="case">
                                    {translations.meanings[key].cases ? translations.meanings[key].cases[item.state] : ''}
                                </div>
                                <div
                                    className={item.state ?
                                        'label label-success' :
                                        !item.state ?
                                            'label label-danger' :
                                            'label label-default'
                                    }>{item.state.toString()}</div>
                            </span>;
                            });

                            return <TableRow
                                key={listItemIndex}>
                                <TableRowColumn>{meaning["key"]}</TableRowColumn>
                                <TableRowColumn>
                                    {translations.meanings[key].translation}
                                </TableRowColumn>
                                <TableRowColumn>{meaning["type"]}</TableRowColumn>
                                <TableRowColumn>Boolean</TableRowColumn>
                                <TableRowColumn>{cases}</TableRowColumn>
                            </TableRow>;
                        default:
                            throw new Error(`No valid type: ${meaning.type}`)
                    }
                }) || [];

        return (
            <Table
                selectable={false}>
                <TableHeader
                    displaySelectAll={false}
                    adjustForCheckbox={false}>
                    <TableRow>
                        <TableHeaderColumn>Key</TableHeaderColumn>
                        <TableHeaderColumn>Title</TableHeaderColumn>
                        <TableHeaderColumn>Type</TableHeaderColumn>
                        <TableHeaderColumn>Unit</TableHeaderColumn>
                        <TableHeaderColumn>Value</TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody
                    displayRowCheckbox={false}>
                    {listItems}
                </TableBody>
            </Table>
        );
    }
}

const mapStateToProps = state => {
    const {meanings} = state;
    return {
        meanings: meanings.all,
        translations: state.translations[state.translations.langKey]
    }
};

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Meanings);