import React from 'react';
import {
    AutoComplete,
    TextField
} from 'material-ui';

export default function Basics(props) {
    const {
        styles,
        makes,
        options,
        models,
        years,
        onMakeChange,
        onModelChange,
        onVinChange,
        onYearChange
    } = props;

    return <div style={styles.split}>
        <div style={styles.half}>
            <AutoComplete
                floatingLabelText="Make"
                filter={AutoComplete.caseInsensitiveFilter}
                dataSource={makes.map(item => item.name)}
                searchText={options.make}
                openOnFocus={true}
                fullWidth={true}
                onUpdateInput={onMakeChange}
                maxSearchResults={5}
            />
        </div>
        <div style={styles.half}>
            <AutoComplete
                floatingLabelText="Model"
                filter={AutoComplete.caseInsensitiveFilter}
                dataSource={models.map(item => item.name)}
                onUpdateInput={onModelChange}
                searchText={options.model}
                openOnFocus={true}
                fullWidth={true}
                maxSearchResults={5}
            />
        </div>
        <div style={styles.half}>
            <AutoComplete
                floatingLabelText="Year"
                filter={AutoComplete.caseInsensitiveFilter}
                dataSource={years.map(item => item.year.toString())}
                onUpdateInput={onYearChange}
                searchText={options.year}
                openOnFocus={true}
                fullWidth={true}
                maxSearchResults={5}
            />
        </div>
        <div style={styles.half}>
            <TextField
                fullWidth={true}
                floatingLabelText="VIN"
                value={options.vin}
                onChange={onVinChange}
            />
        </div>
    </div>
}