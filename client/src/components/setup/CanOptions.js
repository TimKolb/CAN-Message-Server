import React from 'react';
import {
    AutoComplete,
    TextField,
    Toggle
} from 'material-ui';

export default function CanOptions(props) {
    const {
        styles,
        options,
        onBusTypeChange,
        onBusOriginChange,
        onBaudrateChange,
        onExtendedAddressChange,
        onPinChange
    } = props;

    return <div style={styles.split}>
        <div style={styles.half}>
            <AutoComplete
                floatingLabelText="Bus type"
                filter={AutoComplete.caseInsensitiveFilter}
                dataSource={[]}
                searchText={options.busType}
                openOnFocus={true}
                fullWidth={true}
                onUpdateInput={onBusTypeChange}
                maxSearchResults={5}
            />
        </div>
        <div style={styles.half}>
            <AutoComplete
                floatingLabelText="Bus origin"
                filter={AutoComplete.caseInsensitiveFilter}
                dataSource={['powertrain', 'comfort']}
                searchText={options.busOrigin}
                openOnFocus={true}
                fullWidth={true}
                onUpdateInput={onBusOriginChange}
                maxSearchResults={5}
            />
        </div>
        <div style={styles.half}>
            <TextField
                fullWidth={true}
                floatingLabelText="Baudrate"
                value={options.baudrate}
                onChange={onBaudrateChange}
                type="number"
                pattern="\d*"
            />
        </div>
        <div style={{...styles.half, marginTop: '40px'}}>
            <Toggle
                label={options.extendedAddress ? 'Extended Address' : 'Basic Address'}
                labelPosition="right"
                toggled={options.extendedAddress}
                onToggle={onExtendedAddressChange}
            />
            <Toggle
                label={`Pin ${options.pin}`}
                labelPosition="right"
                toggled={options.pin === 'high'}
                onToggle={onPinChange}
            />
        </div>
    </div>
}