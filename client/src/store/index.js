import {createStore, applyMiddleware, compose} from 'redux';
import {createLogger} from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import reducers from '../reducers';
import {syncHistoryWithStore} from 'react-router-redux';
import {routerMiddleware} from 'react-router-redux'
import {browserHistory} from 'react-router';

import {
    RECEIVE_MESSAGE,
    getFormulas,
    getMeanings,
    getMakes,
    getTranslations,
    getUnits,
    getOptions,
    receiveMessage,
    restoreFilters,
    socket
} from '../actions';
//import DevTools from '../container/DevTools';

const loggerMiddleware = createLogger({
    predicate: (getState, action) => action.type !== RECEIVE_MESSAGE
});

const enhancer = compose(
    // Required! Enable Redux DevTools with the monitors you chose
    applyMiddleware(
        thunkMiddleware, // lets us dispatch() functions
        routerMiddleware(browserHistory),
        //loggerMiddleware // neat middleware that logs actions
    ),
    //DevTools.instrument()
);

function configureStore(initialState) {
    // See https://github.com/rackt/redux/releases/tag/v3.1.0
    const store = createStore(reducers, initialState, enhancer);

    // Hot reload reducers (requires Webpack or Browserify HMR to be enabled)
    if (module.hot) {
        module.hot.accept('../reducers', () =>
            store.replaceReducer(require('../reducers')/*.default if you use Babel 6+ */)
        );
    }

    return store;
}

const store = configureStore({});

export const history = syncHistoryWithStore(browserHistory, store);

socket.on('connect', () => {
    store.dispatch(getMeanings());
    store.dispatch(getTranslations());
    store.dispatch(getUnits());
    store.dispatch(getFormulas());
    store.dispatch(getMakes());
    store.dispatch(getOptions());
    store.dispatch(restoreFilters());
});

socket.on('message', message => {
    store.dispatch(receiveMessage(message));
});

export default store;