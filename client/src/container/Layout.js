import React from 'react';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import * as colors from 'material-ui/styles/colors';
//import DevTools from './DevTools';

const muiTheme = getMuiTheme({
        ...darkBaseTheme,
        fontFamily: 'Roboto, sans-serif',
        palette: {
            primary1Color: colors.blueGrey700,
            primary2Color: colors.blueGrey500,
            primary3Color: colors.blueGrey100,
            accent1Color: colors.lightGreen500,
            accent2Color: colors.lightGreen500,
            accent3Color: colors.lightGreen500,
            textColor: colors.fullWhite,
            secondaryTextColor: colors.grey500,
            alternateTextColor: colors.grey100,
            canvasColor: '#303030',
            borderColor: colors.grey100,
            disabledColor: colors.grey200,
            pickerHeaderColor: colors.grey300,
            clockCircleColor: colors.grey300
        }
    }
);

export default class Layout extends React.Component {
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    };

    render() {
        return (
            <MuiThemeProvider muiTheme={muiTheme} style={{height: '100%'}}>
                <div style={{height: '100%'}}>
                    {this.props.children}
                </div>
            </MuiThemeProvider>
        );
    }
}
