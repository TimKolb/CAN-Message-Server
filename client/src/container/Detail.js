import React from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';
import {
    AppBar,
    IconButton,
} from 'material-ui';
import ArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import DataLineCharts from '../components/detail/DataLineCharts';
import MetaCharts from '../components/detail/MetaCharts';
import MapMeaning from '../components/detail/MapMeaning';
import ECU from '../components/common/ECU';


const styles = {
    container: {
        display: 'flex',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
        paddingBottom: '20px'
    }
};

class Detail extends React.Component {
    static defaultProps = {
        messages: []
    };

    static propTypes = {
        messages: React.PropTypes.array.isRequired,
        dispatch: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        const initialWidth = window.innerWidth > 0 ? window.innerWidth : 500;
        this.state = {
            windowWidth: initialWidth - 100,
            selectedByte: {index: 0},
            selectedBytes: []
        };
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }

    handleResize = () => {
        this.setState({windowWidth: window.innerWidth - 100});
    };

    toMain = () => {
        const {dispatch} = this.props;
        dispatch(push('/'));
    };

    render() {
        const {ecu, messages} = this.props;
        const {windowWidth} = this.state;
        let message = {
            ecu: 0xFF,
            data: [
                {value: 0xFF, marked: false},
                {value: 0xFF, marked: false},
                {value: 0xFF, marked: false},
                {value: 0xFF, marked: false},
                {value: 0xFF, marked: false}
            ],
            visible: true,
            timestamp: 0,
            sequencenr: 0,
            interval: 0
        };

        let metaCharts = <div>No data</div>;
        let dataCharts = null;

        if (messages.length > 0) {
            message = messages[messages.length - 1];
            metaCharts = <MetaCharts messages={messages} style={styles.container}/>;

            if (messages.length > 1) {
                dataCharts = <DataLineCharts style={styles.container} messages={messages} windowWidth={windowWidth}/>;
            }
        }

        return <div>
            <AppBar
                title={<span>ECU <ECU value={parseInt(ecu, 10)}/></span>}
                iconElementLeft={<IconButton onTouchTap={this.toMain}>
                    <ArrowLeft />
                </IconButton>}
            />
            <MapMeaning
                ecu={parseInt(ecu, 10)}
                message={message}
                position={this.state.selectedByte}/>
            <div style={styles.container}>
                {dataCharts}
            </div>
            {metaCharts}
        </div>;
    }
}


const makeMapStateToProps = (initialState, initialProps) => {
    const {ecu} = initialProps.params;
    return state => ({
        ecu,
        messages: state.stream.messages[ecu]
    });
};

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    makeMapStateToProps,
    mapDispatchToProps
)(Detail);