import React from 'react';
import {
    Tabs,
    Tab
} from 'material-ui';
import LiveTable from '../components/live/liveTable/LiveTable';
import Meanings from '../components/live/meanings/Meanings';
import BubbleView from '../components/live/bubbleView/BubbleView';

const styles = {
    tabButton: {
        color: '#FFF'
    }
};

export default class Live extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            open: false,
            tabIndex: 'table'
        };
    }

    handleChange = (index) => {
        this.setState({
            tabIndex: index,
        });
    };

    render() {
        return (
            <main>
                <Tabs
                    contentContainerStyle={styles.contentContainer}
                    value={this.state.tabIndex}
                    onChange={this.handleChange}
                >
                    <Tab style={styles.tabButton} label="Bubbles" value="bubbles">
                        <BubbleView />
                    </Tab>
                    <Tab style={styles.tabButton} label="Table" value="table">
                        <LiveTable active={this.state.tabIndex === 'table'}/>
                    </Tab>
                    <Tab style={styles.tabButton} label="Meanings" value="meanings">
                        <Meanings />
                    </Tab>
                </Tabs>

            </main>
        );
    }
}