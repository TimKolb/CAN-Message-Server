import React from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';
import {
    Card,
    CardActions,
    CardHeader,
    CircularProgress,
    FlatButton,
    RaisedButton,
    Step,
    Stepper,
    StepButton,
} from 'material-ui';
import ArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import {setOptions} from '../actions';

import Basics from '../components/setup/Basics';
import CanOptions from '../components/setup/CanOptions';

const styles = {
    backButton: {
        alignSelf: 'flex-start'
    },
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        paddingBottom: '25px',
        flexBasis: '600px',
        flexShrink: 0,
        flexGrow: 1,
    },
    card: {
        padding: '25px 0',
        width: '600px',
        maxWidth: '100%'
    },
    split: {
        display: 'flex',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
        paddingBottom: '35px'
    },
    half: {
        flexBasis: '40%'
    }
};

class Setup extends React.Component {

    static defaultProps = {
        options: {},
        makes: [],
        years: []
    };

    constructor(props) {
        super(props);

        this.state = {
            options: props.options,
            models: this.computeModels(props.makes),
            finished: false,
            stepIndex: 0
        };
    }

    componentWillUpdate(nextProps) {
        if (this.props.isFetchingOptions && !nextProps.isFetchingOptions) {
            this.setState({options: nextProps.options});
        }
    }

    computeModels(makes) {
        let models = makes.map(make => make.models);
        if (Array.isArray(models) && models.length > 0) {
            models = models.reduce((pre, cur) => Array.isArray(pre) && Array.isArray(cur) ? pre.concat(cur) : pre)
        }
        return models;
    }

    handleNext = () => {
        const {stepIndex} = this.state;
        this.setState({
            stepIndex: stepIndex + 1,
            finished: stepIndex + 1 >= 1,
        });
    };

    handlePrev = () => {
        const {stepIndex} = this.state;
        if (stepIndex > 0) {
            this.setState({stepIndex: stepIndex - 1});
        }
    };

    toMain = () => {
        const {dispatch} = this.props;
        dispatch(push(`/`));
    };

    onSubmit = () => {
        const {options} = this.state;
        const {dispatch} = this.props;
        dispatch(setOptions(options));
        dispatch(push('/'));
    };

    onMakeChange = make => {
        const {options} = this.state;
        const {makes} = this.props;
        if (!makes) {
            return;
        }
        const selectedMake = makes.find(item => item.name === make);
        this.setState({
            options: {
                ...options,
                make
            },
            makes,
            models: (selectedMake && selectedMake.models) ? selectedMake.models : [],
        });
    };

    onModelChange = model => {
        const {models, options} = this.state;
        const selectedModel = models.find(item => item.name === model);
        this.setState({
            options: {
                ...options,
                model
            },
            years: (selectedModel && selectedModel.years) ? selectedModel.years : []
        });
    };

    onYearChange = year => {
        const {options} = this.state;
        this.setState({
            options: {
                ...options,
                year
            }
        });
    };

    onVinChange = (event, vin) => {
        const {options} = this.state;
        this.setState({
            options: {
                ...options,
                vin
            }
        });
    };

    onBusTypeChange = busType => {
        const {options} = this.state;
        this.setState({
            options: {
                ...options,
                busType
            }
        });
    };

    onBusOriginChange = busOrigin => {
        const {options} = this.state;
        this.setState({
            options: {
                ...options,
                busOrigin
            }
        });
    };

    onBaudrateChange = (event, baudrate) => {
        const {options} = this.state;
        this.setState({
            options: {
                ...options,
                baudrate: parseInt(baudrate, 10)
            }
        });
    };

    onExtendedAddressChange = (event, extendedAddress) => {
        const {options} = this.state;
        this.setState({
            options: {
                ...options,
                extendedAddress
            }
        });
    };

    onPinChange = (event, pin) => {
        const {options} = this.state;
        this.setState({
            options: {
                ...options,
                pin: pin ? 'high' : 'low'
            }
        });
    };

    getStepContent(stepIndex) {
        const {
            options,
            models,
        } = this.state;

        const {
            makes,
            years
        } = this.props;

        switch (stepIndex) {
            case 0:
                return (<Basics styles={styles}
                                makes={makes}
                                options={options}
                                models={models}
                                years={years}
                                onMakeChange={this.onMakeChange}
                                onModelChange={this.onModelChange}
                                onVinChange={this.onVinChange}
                                onYearChange={this.onYearChange}/>);
            case 1:
                return (<CanOptions styles={styles}
                                    options={options}
                                    onBusTypeChange={this.onBusTypeChange}
                                    onBusOriginChange={this.onBusOriginChange}
                                    onBaudrateChange={this.onBaudrateChange}
                                    onExtendedAddressChange={this.onExtendedAddressChange}
                                    onPinChange={this.onPinChange}/>);
            default:
                return 'You\'re a long way from home sonny jim!';
        }
    }

    render() {
        const {stepIndex, finished} = this.state;
        const {
            isFetchingOptions,
            isFetchingMakes
        } = this.props;
        const isLoading = isFetchingOptions || isFetchingMakes;

        return (
            <div style={styles.container}>
                <FlatButton
                    style={styles.backButton}
                    onTouchTap={this.toMain}
                    label="Back"
                    primary={true}
                    icon={<ArrowLeft />}
                />
                <Card style={styles.card}>
                    <CardHeader
                        title="Setup"
                        subtitle="Enter information for this session"
                    />
                    {isLoading ?
                        <div style={{display: 'flex', justifyContent: 'center'}}>
                            <CircularProgress size={60} thickness={7}/>
                        </div>
                        :
                        <div>
                            <CardActions>
                                <Stepper linear={false} activeStep={stepIndex}>
                                    <Step>
                                        <StepButton onClick={this.handlePrev}>
                                            <span style={{color: '#FFF'}}>Basics</span>
                                        </StepButton>
                                    </Step>
                                    <Step>
                                        <StepButton onClick={this.handleNext}>
                                            <span style={{color: '#FFF'}}>CAN Options</span>
                                        </StepButton>
                                    </Step>
                                </Stepper>
                            </CardActions>

                            <CardActions>
                                <div>{this.getStepContent(stepIndex)}</div>
                                <div style={{marginTop: 12}}>
                                    <FlatButton
                                        label="Back"
                                        disabled={stepIndex === 0}
                                        onTouchTap={this.handlePrev}
                                        style={{marginRight: 12}}
                                    />
                                    <RaisedButton
                                        label={finished ? 'Finish' : 'Next'}
                                        primary={true}
                                        onTouchTap={finished ? this.onSubmit : this.handleNext}
                                    />
                                </div>
                            </CardActions>
                        </div>}
                </Card>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const {
        options,
        makes,
        isFetchingOptions,
        isFetchingMakes
    } = state.settings;

    return {
        options,
        makes,
        years: [],
        isFetchingOptions,
        isFetchingMakes
    };
};

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Setup);