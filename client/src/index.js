import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Router, Route, IndexRoute} from 'react-router';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import store, {history} from './store';
import Layout from './container/Layout';
import Detail from './container/Detail';
import Setup from './container/Setup';
import Live from './container/Live';

import injectTapEventPlugin from 'react-tap-event-plugin';

import './styles/index.css';

injectTapEventPlugin();

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <ReactCSSTransitionGroup
                transitionName="fade"
                transitionEnterTimeout={300}
                transitionLeaveTimeout={300}>
                <Route path='/' component={Layout}>
                    <IndexRoute component={Live}/>
                    <Route path="setup" component={Setup}/>
                    <Route path="ecu/:ecu" component={Detail}/>
                </Route>
            </ReactCSSTransitionGroup>
        </Router>
    </Provider>,
    document.getElementById('root')
);
