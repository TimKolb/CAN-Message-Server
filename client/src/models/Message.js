export default class Message {
    constructor({ecu, data, visible, timestamp, sequencenr, interval, found}) {
        this.ecu = ecu;
        this.data = data;
        this.visible = !!visible;
        this.timestamp = timestamp;
        this.sequencenr = sequencenr;
        this.interval = interval;
        this.found = found;
    }
}