import Meaning from './Meaning';

export default class FunctionMeaning extends Meaning {
    constructor({unit, stringType, formula, ...rest}) {
        super(rest);
        this.unit = unit;
        this.stringType = stringType;
        this.formula = formula;
    }
}