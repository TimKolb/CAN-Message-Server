export const FUNCTION = 'FUNCTION';
export const SWITCH = 'SWITCH';

export default class Meaning {
    constructor({_id, key, type, addedManual}) {
        this._id = _id;
        this.key = key;
        this.type = type;
        this.addedManual = !!addedManual;
    }
}