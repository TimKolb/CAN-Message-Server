import Meaning from './Meaning';
import SwitchMeaningCase from './SwitchMeaningCase';

export default class SwitchMeaning extends Meaning {
    cases = [];

    constructor({cases, ...rest}) {
        super(rest);
        this.cases = Array.isArray(cases) && cases.map(item => new SwitchMeaningCase(item));
    }

    addCase(newCase) {
        if (!newCase) {
            return this;
        }
        this.cases.push(new SwitchMeaningCase(newCase));
        return this;
    }
}