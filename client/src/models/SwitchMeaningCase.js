export default class SwitchMeaningCase {
    translations = {};

    constructor({state, translations, value, ...rest}) {
        this.state = state;
        this.translations = translations;
        this.value = value;
        this.rest = Object.keys(rest).length > 0 ? rest : undefined;
    }
}